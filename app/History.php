<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $guarded=[];

    public function structure()
    {
        return $this->belongsTo('App\Structure');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
