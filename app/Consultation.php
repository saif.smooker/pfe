<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consultation extends Model
{
    protected $guarded=[];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function consultationPrearchive($companyid)
    {
        return Consultation::join('users','users.id','orders.user_id')
            ->where('users.company_id',$companyid);
    }
}
