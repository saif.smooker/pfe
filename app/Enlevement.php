<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enlevement extends Model
{
    protected $guarded=[];
    protected $dates = [
        'created_at',
        'updated_at',
        'date',
        // your other new column
    ];
    public function documents()
    {
        return $this->hasMany('App\Document');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function structure()
    {
        return $this->belongsTo('App\Structure');
    }
}
