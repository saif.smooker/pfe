<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prelevement extends Model
{
    protected $guarded=[];

    public function documents()
    {
        return $this->hasMany('App\Document');
    }
}
