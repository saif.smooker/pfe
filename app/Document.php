<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $dates = [
        'created_at',
        'updated_at',
        'age1',
        'age2',
        'age3',
        // your other new column
    ];
    protected $guarded=[];
    public function nomenclature()
    {
        return $this->belongsTo('App\Nomenclature');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function versement()
    {
        return $this->belongsTo('App\Versement');
    }
    public function time($id)
    {
        $document=Document::find($id)->first();
        $dt=Carbon::now();
        if($dt->greaterThanOrEqualTo($document->created_at->addMinute()))
            return 0;
        return 1;
    }
}
