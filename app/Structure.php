<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Structure extends Model
{
    protected $guarded=[];

    public function users()
    {
        return $this->hasMany('App\User');
    }

    public function region()
    {
        return $this->belongsTo('App\Region');
    }

    public function prearchive()
    {
        return $this->belongsTo('App\Prearchive');
    }
    public function type()
    {
        return $this->belongsTo('App\Type');
    }
    public function age2($id)
    {
        return Structure::find($id);
    }
    public function age3($id)
    {
        return Structure::find($id);
    }

}
