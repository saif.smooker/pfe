<?php

namespace App\Http\Controllers;

use App\Document;
use App\Enlevement;
use App\History;
use App\Nomenclature;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DocumentController extends Controller
{

    public function index()
    {
        $date =today()->format('Y-m-d');
        $user=Auth::user();
        if( ($user->structure->type->id== 1) || ($user->structure->type->id== 2) || ($user->structure->type->id == 3) )
        {
            Document::where('age1', '<', $date)->update(['traitement' =>'1']);
            $documents = Document::where('statut', '1')->where('user_id',$user->id);
        }

        elseif($user->structure->type->id== 4)
        {
            $date=today()->format('Y-m-d');
//            $enlevement=Document::join('enlevements','documents.structure_id','enlevements.structure_id')
//                ->where('enlevements.date','<',$date)
//                ->where('documents.enlevement','=',1)
//                ->where('documents.traitement','!=',1)
//                ->where('enlevements.statut','1');
//            $enlevement->update(['documents.statut'=>'2','documents.enlevement'=>0,'traitement'=>'0']);

            Document::where('age2', '<', $date)->update(['traitement' =>'1']);
            $documents = Document::where('statut','=', '2')->where('enlevement',0);
        }

        else
        {
//            $enlevement=Document::join('enlevements','documents.enlevement_id','enlevements.id')
//                ->where('enlevements.date','<=',$date)
//                ->where('documents.enlevement',1)
//                ->where('enlevements.statut',2);
////            dd($enlevement->get());
//            $enlevement->update(['documents.statut'=>'3','documents.enlevement'=>0,'traitement'=>'0']);
            Document::where('age3', '<', $date)->where('traitement',0)->update(['traitement' =>'1']);
            $documents = Document::where('statut','3')->where('enlevement',0);
        }

        $documents = $documents->whereIn('traitement',['0','2'])->get();
//        dd($documents);

        return view('documents.index',compact('documents'));
    }


    public function create()
    {
        $nomenclatures=Nomenclature::all();
        return view('documents.create',compact('nomenclatures'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'num' => 'required|unique:documents',
            'pages' => 'required',
            'nbrdoc' => 'required',
            'chapitre' => 'required',
//            'nomenclature_id' => 'required',
//            'type_id' => 'required',
//            'structure_id' => 'required',
        ]);
        $document=new Document();
        $document->num=$request->num;
        $document->pages=$request->pages;
        $document->nbrdoc=$request->nbrdoc;
        $document->chapitre=$request->chapitre;
        $document->nomenclature_id=$request->nomenclature;
        $document->user_id=Auth::user()->id;
        $document->structure_id=Auth::user()->structure_id;
        $dt=Carbon::now();
        $nomenclature=Nomenclature::find($request->nomenclature);
        $age1=json_decode($nomenclature->age1);
        $age2=json_decode($nomenclature->age2);
        $age3=json_decode($nomenclature->age3);

        $document->age1=$dt->addYears($age1->year);
        $document->age1=$dt->addMonths($age1->month);
        $document->age1=$dt->addDays($age1->day);
        $dt1=$document->age1;
        $document->save();
        $document->age2=$dt1->addYears(($age2->year));
        $document->age2=$dt1->addMonths($age2->month);
        $document->age2=$dt1->addDays(($age2->day));
        $dt2=$document->age2;
        $document->save();
        $document->age3=$dt2->addYears($age3->year);
        $document->age3=$dt2->addMonths($age3->month);
        $document->age3=$dt2->addDays($age3->day);

        $document->save();
        History::create([
            'structure_id'=>Auth::user()->structure->id,
            'user_id'=>Auth::id(),
            'action'=>'Création d\'un document'
        ]);
//        dd($document);
        return redirect(route('document.index'))->with('message','Document '.$document->num.' a été crée avec succès ');
    }


    public function show(Document $document)
    {
        return view('documents.show',compact('document'));
    }

    public function edit(Document $document)
    {
        $nomenclatures=Nomenclature::all();
        return view('documents.edit',compact('document','nomenclatures'));
    }


    public function update(Request $request, Document $document)
    {
        $request->validate([
            'num' => 'required|unique:documents',
            'pages' => 'required',
            'nbrdoc' => 'required',
            'chapitre' => 'required',
//            'nomenclature_id' => 'required',
//            'type_id' => 'required',
//            'structure_id' => 'required',
        ]);
        $document->num=$request->num;
        $document->pages=$request->pages;
        $document->nbrdoc=$request->nbrdoc;
        $document->chapitre=$request->chapitre;
        $document->nomenclature_id=$request->nomenclature;
        $document->user_id=Auth::user()->id;
        $document->structure_id=Auth::user()->structure_id;
        $dt=Carbon::now();
        $nomenclature=Nomenclature::find($request->nomenclature);
        $age1=json_decode($nomenclature->age1);
        $age2=json_decode($nomenclature->age2);
        $age3=json_decode($nomenclature->age3);

        $document->age1=$dt->addYears($age1->year);
        $document->age1=$dt->addMonths($age1->month);
        $document->age1=$dt->addDays($age1->day);
//        $dt=$document->age1;
        $document->age2=$dt->addYears(int($age2->year+$age1->year));
        dd();
        $document->age2=$dt->addMonths($age2->month+$age1->month);
        $document->age2=$dt->addDays($age2->day+$age1->day);
//        $dt=$document->age2;
        $document->age3=$dt->addYears($age3->year+$age2->year);
        $document->age3=$dt->addMonths($age3->month+$age2->year);
        $document->age3=$dt->addDays($age3->day+$age2->year);
        $document->update();
        History::create([
            'structure_id'=>Auth::user()->structure->id,
            'user_id'=>Auth::id(),
            'action'=>'Modification d\'un document'
        ]);
        return redirect(route('document.index'))->with('message','Document '.$document->num.' a été modifiée avec succès ');;
    }


    public function destroy(Document $document)
    {
        $num=$document->num;
        $document->delete();
        History::create([
            'structure_id'=>Auth::user()->structure->id,
            'user_id'=>Auth::id(),
            'action'=>'Suppression d\'un document'
        ]);
        return redirect(route('document.index'))->with('message','Document '.$num.' a été supprimée avec succès ');
    }
}
