<?php

namespace App\Http\Controllers;

use App\Document;
use App\Versement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DemandeController extends Controller
{
    public function index()
    {
        $user=Auth::user();
        if( ($user->structure->type->id== 1) || ($user->structure->type->id== 2) || ($user->structure->type->id == 3) )
        {
            //les versement de cette structure
            $versements = Versement::join('documents', 'documents.id','versements.document_id')
                ->join('structures','documents.structure_id','structures.id')
                ->where('documents.statut',1)
                ->where('documents.enlevement',0)
                ->where('structures.id',Auth::user()->structure_id)
                ->select('versements.*')
                ->get();
            $text='1';
//            dd($versements);
        }
        elseif($user->structure->type->id== 4)
        {
            //les versement de cette structure
            $versements = Versement::join('documents', 'documents.id','versements.document_id')
                ->join('structures','documents.structure_id','structures.id')
                ->where('documents.statut',2)
                ->where('documents.enlevement',0)
                ->where('structures.age2',Auth::user()->structure->id)
                ->select('versements.*')
                ->get();
            $text='2';
        }
        else
        {
            //les versement de cette structure
            $age2=
            $versements = Versement::join('documents', 'documents.id','versements.document_id')
                ->join('structures','documents.structure_id','structures.id')
                ->where('documents.statut',3)
                ->where('documents.enlevement',0)
                ->where('structures.age3',Auth::user()->structure->id)
                ->select('versements.*','structures.age3')
                ->get();
//            dd($versements);
            $text='3';
        }

        return view('demandes.index',compact('versements','text'));
    }

    public function decision(Request $request,$id)
    {
        $versement = Versement::find($id);
        $user=Auth::user();
//        dd($versement);
        if($user->structure->type->id== 5)
        {
//            dd($versement);
            if ($request->decision == 0){
                if ($versement->decision==1){
                    Document::find($versement->document_id)->delete();
                }
                elseif ($versement->decision==0){
                   Document::find($versement->document_id)->update(['traitement'=>'2','versement_id'=>null]);
                }
            }
            if ($request->decision == 1){
                if ($versement->decision==0){
                    Document::find($versement->document_id)->delete();
                }
                elseif ($versement->decision==1){
                    Document::find($versement->document_id)->update(['traitement'=>'2','versement_id'=>null]);
                }
            }
        }
        else
        {
            if ($request->decision == 0){
                if ($versement->decision==1){
                    Document::find($versement->document_id)->delete();
                }
                elseif ($versement->decision==0){
                    $doc=Document::find($versement->document_id);
                    $doc->update(['statut'=>$doc->statut+1,'traitement'=>'0','versement_id'=>null]);
                }
            }
            if ($request->decision == 1){
                if ($versement->decision==0){
                    Document::find($versement->document_id)->delete();
                }
                elseif ($versement->decision==1){
                    $doc=Document::find($versement->document_id);
                    $doc->update(['statut'=>$doc->statut+1,'traitement'=>'0','versement_id'=>null]);
                }
            }
        }

        $versement->delete();
        return redirect()->back();


    }

}
