<?php

namespace App\Http\Controllers;

use App\Prearchive;
use App\Region;
use App\Structure;
use App\Type;
use Illuminate\Http\Request;

class StructureController extends Controller
{

    public function index($type_id)
    {
        $structures=Structure::where('type_id','=',$type_id)->get();
        $type=Type::find($type_id);
        return view('structures.index',compact('structures','type'));
    }

    public function create($type_id)
    {
        $prearchives=Structure::where('type_id','=','4')->get();
        $archives=Structure::where('type_id','=','5')->get();
        $regions=Region::all();
        $type=Type::find($type_id);
        return view('structures.create',compact('type_id','type','prearchives','regions','archives'));
    }


    public function store(Request $request,$type_id)
    {
        $request->validate([
            'nom' => 'required',
            'code' => 'required',
            'adresse' => 'required',
//            'age1' => 'required',
//            'age2' => 'required',
//            'type_id' => 'required',
//            'region_id' => 'required',
        ]);
        $structure=new Structure();
        $structure->nom=$request->nom;
        $structure->code=$request->code;
        $structure->adresse=$request->adresse;
        $structure->age1=$request->age1;
        $structure->age2=$request->prearchive;
        $structure->age3=$request->archive;
        if($type_id==1||$type_id==2||$type_id==3)
        {
            $age3=Structure::find($request->prearchive);
            $structure->age3=$age3->age3;
        }
        $structure->type_id=$type_id;
        $structure->region_id=$request->region;
        $structure->save();

        return redirect(route('structure.index',compact('type_id')))->with('message','la structure '.$structure->name.' a été crée avec succès ');
    }


    public function show($type_id,$id)
    {
        $structure=Structure::find($id);
        $type=Type::find($type_id);
        return view('structures.show',compact('structure','type'));
    }

    public function edit($type_id,Structure $structure)
    {
        $regions=Region::all();
        $type=Type::find($type_id);
        $prearchives=Structure::where('type_id','=','4')->get();
        return view('structures.edit',compact('structure','type','regions','prearchives'));
    }

    public function update(Request $request,$type_id, Structure $structure)
    {
        $request->validate([
            'nom' => 'required',
            'code' => 'required',
            'adresse' => 'required',
//            'age1' => 'required',
//            'age2' => 'required',
//            'type_id' => 'required',
//            'region_id' => 'required',
        ]);
        $structure->nom=$request->nom;
        $structure->code=$request->code;
        $structure->adresse=$request->adresse;
        $structure->age1=$request->age1;
        $structure->age2=$request->prearchive;
        $structure->age3=$request->archive;
        $structure->type_id=$type_id;
        $structure->region_id=$request->region;
        $structure->save();
        return redirect(route('structure.index',$type_id))->with('message','la structure '.$structure->name.' a été modifiée avec succès ');
    }

    public function destroy($type_id,Structure $structure)
    {
        $nom=$structure->nom;
        $structure->delete();
        return redirect()->back()->with('message','la structure '.$nom.' a été supprimée avec succès ');
    }
}
