<?php

namespace App\Http\Controllers;

use App\Consultation;
use App\Document;
use App\History;
use App\Structure;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ConsultationController extends Controller
{

    public function index()
    {
        $age2=Auth::user()->structure->id;
        if(Auth::user()->structure->type->id==4)
        {
            $consultations=Consultation::join('structures','structures.id','consultations.structure_id')
//                ->join('structures','users.structure_id','structures.id')
                ->where('structures.age2',$age2)
                ->select('consultations.*')
                ->get();
//                    dd($consultations);
        }
        else
            $consultations=Consultation::where('user_id',Auth::user()->id)->get();

        return view('consultations.index',compact('consultations'));
    }


    public function create()
    {
        return view('consultations.create');
    }


    public function store(Request $request)
    {

        $consultation=new Consultation();
        $document=Document::where('structure_id',Auth::user()->structure->id)
            ->where('statut',2);
        if($document->where('num',$request->num)->count()==0)
        {
          return redirect()->back()->with('error','numéro introuvable');
        }
        else
        $consultation->num=$request->num;
        $consultation->raison=$request->raison;
        $consultation->objet=$request->objet;
        $consultation->decision=0;
        $consultation->statut=0;
        $consultation->user_id=Auth::user()->id;
        $consultation->structure_id=Auth::user()->structure_id;
        $consultation->date=Carbon::now();
        $consultation->save();
      History::create([
            'structure_id'=>Auth::user()->structure->id,
            'user_id'=>Auth::id(),
            'action'=>'Consultation'
        ]);
        return redirect(route('consultation.index'));

   }

    public function decision(Request $request,$id)
    {
        $consultation=Consultation::find($id);
        if($request->decision=="0")
        {
            $consultation->decision="1";
            $consultation->statut=2;
            $consultation->date=$request->date;
            History::create([
                'structure_id'=>Auth::user()->structure->id,
                'user_id'=>Auth::id(),
                'action'=>'Demande de Consultation Refusée'
            ]);
        }
        elseif($request->decision=="1")
        {
            $consultation->decision=1;
            $consultation->statut=1;
            $consultation->date=$request->date;
            History::create([
                'structure_id'=>Auth::user()->structure->id,
                'user_id'=>Auth::id(),
                'action'=>'Demande de Consultation Acceptée'
            ]);
        }
        $consultation->save();

        return redirect(route('consultation.index'));
    }


    public function edit(Consultation $consultation)
    {
        //
    }


    public function update(Request $request, Consultation $consultation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Consultation  $consultation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Consultation $consultation)
    {
        //
    }
}
