<?php

namespace App\Http\Controllers;

use App\Nomenclature;
use Illuminate\Http\Request;

class NomenclatureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nomenclatures=Nomenclature::all();
        return  view('nomenclatures.index',compact('nomenclatures'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return  view('nomenclatures.create');
    }


    public function store(Request $request)
    {
        $nomenclature=new Nomenclature();
        $nomenclature->code=$request->code;
        $nomenclature->designation=$request->designation;
        $nomenclature->age1=json_encode(array('year' => $request->annee1, 'month' => $request->mois1, 'day' => $request->jour1));
        $nomenclature->age2=json_encode(array('year' => $request->annee2, 'month' => $request->mois2, 'day' => $request->jour2));
        $nomenclature->age3=json_encode(array('year' => $request->annee3, 'month' => $request->mois3, 'day' => $request->jour3));
        $nomenclature->save();
        return redirect(route('nomenclature.index'))->with('message','Nomenclature '.$nomenclature->code.' a été crée avec succès ');
    }


    public function show(Nomenclature $nomenclature)
    {
        return view('nomenclatures.show',compact('nomenclature'));

    }

    public function edit(Nomenclature $nomenclature)
    {
        return view('nomenclatures.edit',compact('nomenclature'));
    }

    public function update(Request $request, Nomenclature $nomenclature)
    {
        $nomenclature->code=$request->code;
        $nomenclature->designation=$request->designation;
        $nomenclature->age1=json_encode(array('year' => $request->annee1, 'month' => $request->mois1, 'day' => $request->jour1));
        $nomenclature->age2=json_encode(array('year' => $request->annee2, 'month' => $request->mois2, 'day' => $request->jour2));
        $nomenclature->age3=json_encode(array('year' => $request->annee3, 'month' => $request->mois3, 'day' => $request->jour3));
        $nomenclature->update();
        return redirect(route('nomenclature.index'))->with('message','Nomenclature '.$nomenclature->code.' a été modifiée avec succès ');;
    }

    public function destroy(Nomenclature $nomenclature)
    {
        $code=$nomenclature->code;
        $nomenclature->delete();
        return redirect(route('nomenclature.index'))->with('message','Nomenclature '.$code.' a été supprimée avec succès ');
    }
}
