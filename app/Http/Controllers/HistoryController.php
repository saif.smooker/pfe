<?php

namespace App\Http\Controllers;

use App\History;
use Illuminate\Http\Request;

class HistoryController extends Controller
{

    public function index()
    {
        $historiques=History::all();
        return view('historiques.index',compact('historiques'));
    }


}
