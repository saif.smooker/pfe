<?php

namespace App\Http\Controllers;

use App\Document;
use App\Versement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VersementController extends Controller
{

    public function index()
    {

       // statut 1: 1ere age
       // statut 2: 2eme age
       // statut 3: 3eme age
        $user=Auth::user();
        if( ($user->structure->type->id== 1) || ($user->structure->type->id== 2) || ($user->structure->type->id == 3) )
        {
            //          les document de cet utilisateur 1ere age
            $document1 = Document::where('statut','1');
            $documents=$document1->where('user_id',Auth::user()->id);
            $documents = $documents->where('traitement',1);
            $text='1';
        }
        elseif($user->structure->type->id==4)
        {
//           tout les documents stocker dans ce centre de préarchive 2éme age

            $document=Document::join('structures','documents.structure_id','structures.id')
                ->where('structures.age2',Auth::user()->structure_id)
                ->where('documents.traitement',1)
                ->where('documents.statut',2)
                ->select('documents.*');
            $documents = $document;
            $text='2';

        }
        else
        {
            //           tout les documents stocker dans ce centre d'archive 3éme age
            $document = Document::where('statut', '3');
//            $document->where('user_id',Auth::user()->id)->get();
            $documents = $document->where('traitement',1);
            $text='3';
        }
//        traitement= doit etre traité
            $documents=$documents->where('versement_id',null)->get();
//        dd($documents);
        return view('versements.index',compact('documents','text'));
    }

    public function decision(Request $request )
    {
//        if($versement=Versement::where('document_id',$request->document_id)->get())
//        {
//
//        }
//        else
//        {
                $versement=Versement::create([
                    'document_id'=>$request->document_id,
                    'user_id'=>Auth::id(),
                    'decision'=>$request->decision
                ]);
//        }

        Document::find($request->document_id)->update(['versement'=>1,'versement_id'=>$versement->id]);
//        $document->update();
       return redirect()->back();
    }




}
