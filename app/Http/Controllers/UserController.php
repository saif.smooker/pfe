<?php

namespace App\Http\Controllers;

use App\History;
use App\Structure;
use App\Type;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index()
    {
        $users=User::where('id','!=',1)->get();
        return view('users.index',compact('users'));
    }
    public function create()
    {
        $types=Type::all();
        return view('users.create',compact('types'));
    }
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'matricule' => 'required',
            'password' => 'required',
            'role' => 'required',
        ]);
        $user=new User();
        $user->name=$request->name;
        $user->email=$request->email;
        $user->password=bcrypt($request->password);
        $user->matricule=$request->matricule;
        if($request->structure)
        {
            $user->structure_id=$request->structure;
        }
        if($request->role==0)
            $user->assignRole('employée');
        else
            $user->assignRole('responsable');
//        $prearchive=Structure::find($request->structure);

        $user->save();
        History::create([
            'structure_id'=>Auth::user()->structure->id,
            'user_id'=>Auth::id(),
            'action'=>'Création d\'un Utilisateur'
        ]);
        return redirect(route('user.index'))->with('message','l\'utilisateur '.$user->name.' a été crée avec succès ROLE:'.$user->getRoleNames()->first());
    }

    public function show($id)
    {
        if(Auth::user()->id==1)
        {
            $user=User::find($id);
            $role=$user->getRoleNames()->first();
            $types=Type::all();
            return view('users.show',compact('user','types','role'));
        }
       elseif (Auth::user()->id==$id)
       {
           $user=User::find($id);
           $role=$user->getRoleNames()->first();
           $types=Type::all();
           return view('users.show',compact('user','types','role'));
       }
        else
        {
            return redirect()->back()->with('error','vous n\'avez pas le droit ');
        }

    }
    public function edit($id)
    {
        $user=User::find($id);
        $types=Type::all();
        $role=$user->getRoleNames()->first();
        return view('users.edit',compact('user','types','role'));
    }

    public function update(Request $request,$id)
    {
//        dd($request->all());
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'matricule' => 'required',
            'role' => 'required',
        ]);
        $user=User::find($id);
        $user->name=$request->name;
        $user->email=$request->email;
        if($request->role==0)
            $user->assignRole('employée');
        else
            $user->assignRole('responsable');
        $user->matricule=$request->matricule;
        if($request->structure)
        {
            $user->structure_id=$request->structure;
        }

        $user->update();
        History::create([
            'structure_id'=>Auth::user()->structure->id,
            'user_id'=>Auth::id(),
            'action'=>'Modification d\'un Utilisateur'
        ]);
        return redirect(route('user.index'))->with('message','l\'utilisateur '.$user->name.' a été modifiée avec succès ROLE:'.$user->getRoleNames()->first());
    }
    public function password(Request $request,$id){
        $user=User::find($id);
        if($request->password)
         $user->password=bcrypt($request->password);
        $user->update();
        return redirect(route('user.index'))->with('message','le mot de passe de l\'utilisateur '.$user->name.' a été modifiée avec succès ');
    }
    public function destroy($id)
    {
        $user=User::find($id);
        $name=$user->name;
        $user->delete();
        History::create([
            'structure_id'=>Auth::user()->structure->id,
            'user_id'=>Auth::id(),
            'action'=>'Suppression d\'un Utilisateur'
        ]);
        return redirect(route('user.index'))->with('message','l\'utilisateur '.$user->name.' a été supprimée avec succès ');
    }
    public function StructuresType($type)
    {
      $structures=Structure::where('type_id','=',$type)->get();
      return view('users.ajaxtype',compact('structures'));
    }
}
