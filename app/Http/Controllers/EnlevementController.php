<?php

namespace App\Http\Controllers;

use App\Document;
use App\Enlevement;
use App\History;
use App\Versement;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PhpParser\Comment\Doc;

class EnlevementController extends Controller
{

    public function index()
    {
        // statut 1: 1ere age
        // statut 2: 2eme age
        $user=Auth::user();
        if( ($user->structure->type->id== 1) || ($user->structure->type->id== 2) || ($user->structure->type->id == 3) )
        {
            //          les demandes enlevements de cet Structure
            $enlevement = Enlevement::where('statut','1');
            $enlevements = $enlevement->where('structure_id',$user->structure_id)->get();

        }
        elseif($user->structure->type->id== 4)
        {
//          //          les demandes enlevements de cet Structure
            $enlevement = Enlevement::where('statut','2');
            $enlevements = $enlevement->where('structure_id',$user->structure_id)->get();
//            $text='2';
        }
//        $enlevements=Enlevement::all();
        return view('enlevement.index',compact('enlevements'));
    }


    public function create()
    {
        return view('enlevement.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'code' => 'required',
            'materiel' => 'required',
            'humaines' => 'required',
        ]);
        $enlevement=new Enlevement();
        $enlevement->code=$request->code;
        $enlevement->materiel=$request->materiel;
        $enlevement->humaines=$request->humaines;
        $enlevement->user_id=Auth::user()->id;
        $enlevement->structure_id=Auth::user()->structure_id;

        $enlevement->save();
        History::create([
            'structure_id'=>Auth::user()->structure->id,
            'user_id'=>Auth::id(),
            'action'=>'Création d\'un Plan d\'enlévement '
        ]);
        if(Auth::user()->structure->type->id==4)
        {
            $enlevement->statut=2;
            Document::join('versements','versements.document_id','documents.id')
                ->where('statut','2')->where('decision','1')->update(['enlevement'=>1,'enlevement_id'=>$enlevement->id]);
        }
        else
        {
            $enlevement->statut=1;
            Document::join('versements','versements.document_id','documents.id')
                ->where('statut','1')->where('decision','1')->update(['enlevement'=>1,'enlevement_id'=>$enlevement->id]);

        }
        $enlevement->save();

        return redirect(route('enlevement.index'));
    }

    public function demandes()
    {
        $user=Auth::user();
        if(  ($user->structure->type->id== 4) )
        {
            //          les demandes enlevements de cet Structure

            $enlevement=Enlevement::join('structures','enlevements.structure_id','structures.id')
                ->where('structures.age2',Auth::user()->structure_id)
                ->select('enlevements.*','structures.age2');
            $enlevements = $enlevement->where('statut',1)->get();

        }
        elseif($user->structure->type->id== 5)
        {
//          //          les demandes enlevements de cet Structure
            $enlevement=Enlevement::join('structures','enlevements.structure_id','structures.id')
                ->where('structures.age3',Auth::user()->structure_id)
                ->select('enlevements.*','structures.age3');
            $enlevements = $enlevement->where('enlevements.statut',2)->get();
//            $text='2';
//            dd($enlevements);
        }

        return view('enlevement.demande',compact('enlevements'));
    }

    public function date(Request $request,Enlevement $enlevement)
    {
//        dd($enlevement);
        Versement::join('documents','documents.id','versements.document_id')
            ->where('documents.enlevement_id',$enlevement->id)
            ->delete();
        $document=Document::where('enlevement_id',$enlevement->id)->get();
//        dd($document);
        foreach ($document as $document)
        {
//            ->update(['enlevement'=>0,'traitement'=>0,'versement_id'=>null]
            $document->enlevement=0;
            $document->traitement=0;
            $document->versement_id=null;
            $document->statut=$document->statut+1;
            $document->update();

        }
//        $document->update(['statut'=>$document->]);
        $enlevement->decision=1;
        $date=Carbon::parse($request->date)->format('Y-m-d');
        $enlevement->date=$date;
        $enlevement->etat=1;
        $enlevement->update();
        History::create([
            'structure_id'=>Auth::user()->structure->id,
            'user_id'=>Auth::id(),
            'action'=>'Création d\'une date d\'un Plan d\'enlévement'
        ]);

        return redirect(route('enlevement.demandes'));

    }
    public function calendar()
    {
        $user=Auth::user();
        if(  ($user->structure->type->id== 4) )
        {
            //          les demandes enlevements de cet Structure
            $enlevement=Enlevement::join('structures','enlevements.structure_id','structures.id')
                ->where('structures.age2',Auth::user()->structure_id)
                ->select('enlevements.*','structures.age2');
            $enlevements = $enlevement->where('statut',1)->get();

        }
        elseif($user->structure->type->id== 5)
        {
//          //          les demandes enlevements de cet Structure
            $enlevement=Enlevement::join('structures','enlevements.structure_id','structures.id')
                ->where('structures.age3',Auth::user()->structure_id)
                ->select('enlevements.*','structures.age3');
            $enlevements = $enlevement->where('statut',2)->get();
//            $text='2';
        }

//        dd($enlevements);
        return view('enlevement.calendar',compact('enlevements'));
    }
    public function show(Enlevement $enlevement)
    {
        //
    }


    public function edit(Enlevement $enlevement)
    {
        //
    }

    public function update(Request $request, Enlevement $enlevement)
    {
        //
    }

    public function destroy(Enlevement $enlevement)
    {
        $documents=Document::where('enlevement_id',$enlevement->id)->get();
        foreach ($documents as $document)
        {
            $document->enlevement=0;
            $document->versement=1;
            $document->enlevement_id=null;
            $document->update();
        }

        $enlevement->delete();
        History::create([
            'structure_id'=>Auth::user()->structure->id,
            'user_id'=>Auth::id(),
            'action'=>'Suppression d\'un Plan d\'enlévement'
        ]);
        return redirect(route('demande.index'));
    }
}
