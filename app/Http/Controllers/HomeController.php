<?php

namespace App\Http\Controllers;

use App\Consultation;
use App\Document;
use App\Enlevement;
use App\History;
use App\Structure;
use App\Versement;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $structure=Structure::all();
        $agences=Structure::where('type_id','=','1')->get();
        $regionals=Structure::where('type_id','=','2')->get();
        $centrals=Structure::where('type_id','=','3')->get();
        $prearchives=Structure::where('type_id','=','4')->get();
        $archives=Structure::where('type_id','=','5')->get();
        $historiques=History::all();
        $utilisateurs=User::where('structure_id',Auth::user()->structure_id)->count();
        $age=Auth::user()->structure->type->id==4 ? 2 : Auth::user()->structure->type->id==5 ? 3 : 1 ;
        $documents=Document::where('structure_id',Auth::user()->structure_id)->where('statut',$age)->count();
        $documentage=Document::where('structure_id',Auth::user()->structure_id)->count();
        $versements=Document::where('versement',1)->where('statut',$age)->count();
        $enlevements=Enlevement::where('structure_id',Auth::user()->structure_id)->count();
        $consultations=Consultation::where('structure_id',Auth::user()->structure_id)->count();
//        $traitements=Versement::join('documents','documents.versement_id','versements.id')
//                                ->where('documents.traitement',1)
//                                ->where('versements.structure_id',Auth::user()->structure_id)->get();
           $traitements= Document::where('traitement',1)->where('statut',$age)->count();
        return view('home',compact('agences','regionals','centrals',
            'prearchives','archives','historiques','utilisateurs','documents',
            'documentage','versements','enlevements','consultations','traitements'));
    }
}
