@extends('layout')
@section('nom')

@endsection
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont icofont-user bg-c-blue"></i>
                    <div class="d-inline">
                        <h4>Utilisateurs</h4>
                        <span>Liste des <code style="text-transform: uppercase">Utilisateurs</code></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="/">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Liste des Utilisateurs</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <!-- DOM/Jquery table start -->
                <div class="card">

                    <div class="card-block">
                        <a href="{{route('user.create')}}" type="button" id="addRow" class="btn btn-primary m-b-20 float-lg-right">+ Créer Un Nouvel Utilisateur </a>
                        <div class="table-responsive dt-responsive">
                            <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                <thead>
                                <tr>
                                    <th>nom</th>
                                    <th>matricule</th>
                                    <th>structure</th>
{{--                                    <th>role</th>--}}
{{--                                    <th>adresse</th>--}}
{{--                                    <th>Lieu Conservation Age 2</th>--}}
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{$user->name}}
                                            @if($user->hasrole('admin')) <span class="badge badge-success">admin</span>@endif
                                            @if($user->hasrole('responsable'))<span class="badge badge-info">responsable</span>@endif
                                        </td>
                                        <td>{{$user->matricule}}</td>
                                        <td>{{$user->structure->type->name}}|{{$user->structure->adresse}}</td>
{{--                                        <td>@if($user->hasrole('admin')) admin @elseif($user->hasrole('responsable')) responsable @elseif($user->hasrole('employée')) employée @endif     </td>--}}
{{--                                        <td>{{$user->age1}}</td>--}}
{{--                                        <td>{{$user->prearchive->nom}}</td>--}}
                                        <td>
                                            <a href="{{route('user.show',$user->id)}}" class="badge badge-md bg-success"><i class="fa fa-eye"></i></a>
                                            <a href="{{route('user.edit',$user->id)}}" class="badge badge-md bg-info"><i class="fa fa-pencil"></i></a>
                                            <a href="#" data-toggle="modal" data-target="#delete{{$user->id}}" class="badge badge-md bg-danger"><i class="fa fa-trash"></i></a>

                                        </td>
                                    </tr>
                                    <div class="modal fade" id="delete{{$user->id}}" tabindex="-1" role="dialog">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Confirmation de Suppression</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <h5>Etes vous sûr de vouloir Supprimer cet Utilisateur "{{$user->name}}"?</h5>
                                                </div>
                                                <div class="modal-footer">
                                                    <form id="form" style="display: none;" method="post" action="{{route('user.destroy',$user->id)}}">
                                                        @csrf
                                                        @method('delete')
                                                    <button type="button" class="btn btn-info waves-effect " data-dismiss="modal">Non</button>
                                                    <button type="submit" class="btn btn-danger waves-effect waves-light ">Oui</button>

                                                    </form>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>nom</th>
                                    <th>matricule</th>
                                    <th>structure</th>
{{--                                    <th>role</th>--}}
                                    {{--                                    <th>adresse</th>--}}
{{--                                    <th>Lieu Conservation Age 2</th>--}}
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- DOM/Jquery table end -->
            </div>
        </div>
    </div>
@endsection
