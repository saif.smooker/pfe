@extends('layout')
@section('css')
    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{asset('bower_components/select2/css/select2.min.css')}}"/>
@endsection
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont icofont-user bg-c-blue"></i>
                    <div class="d-inline">
                        <h4>Utilisateurs</h4>
                        <span>Ajouter un<code style="text-transform: uppercase">Utilisateur</code></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="/">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Ajouter Un Nouveau Utilisateur </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <!-- Basic Form Inputs card start -->
                <div class="card">

                    <div class="card-block">
                        <h4 class="sub-title">Formulaire de Création</h4>
                        <form method="post" action="{{route('user.store')}}">
                            @csrf
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Nom</label>
                                <div class="col-sm-10">
                                    <input name="name" type="text" class="form-control" placeholder="Nom Utilisateur">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <input name="email" type="email" class="form-control"  placeholder="Email Utilisateur">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Password</label>
                                <div class="col-sm-10">
                                    <input name="password" type="password" class="form-control" placeholder="Mot de Passe">
                                </div>
                            </div>
{{--                            <div class="form-group row">--}}
{{--                                <label class="col-sm-2 col-form-label">Matricule</label>--}}
{{--                                <div class="col-sm-10">--}}
{{--                                    <input name="matricule" type="text" class="form-control" placeholder="Matricule Utilisateur">--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Matricule</label>
                                <div class="input-group col-sm-10">
                                    <input name="matricule" placeholder="Matricule Utilisateur" type="text" class="form-control" rel="gp" data-size="6" data-character-set="a-z,A-Z,0-9">
                                    <span class="input-group-btn"><button type="button" class="btn btn-default getNewPass"><span class="fa fa-refresh"></span></button></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Role</label>
                                <div class="input-group col-sm-10">
                                    <select name="role" class="form-control">
                                            <option selected value="0">Employée</option>
                                            <option value="1">Responsable</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Type Structure</label>
                                <div class="col-sm-10">
                                    <select id="options" name="type" class="js-example-theme-single form-control">
                                        <option disabled selected >Choisir la structure pour l'utilisateur</option>
                                        @foreach($types as $type)
                                            <option value="{{$type->id}}">{{$type->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row structures">

                            </div>

                            <div class="text-center">
                                <button type="submit" class="col-md-3 btn btn-out-dashed btn-success btn-square">Créer</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- Basic Form Inputs card end -->
            </div>
        </div>

    </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{asset('bower_components/select2/js/select2.full.min.js')}}"></script>
{{--    <script type="text/javascript" src="{{asset('assets/pages/advance-elements/select2-custom.js')}}"></script>--}}
    <script>
        // Generate a password string
        function randString(id){
            var dataSet = $(id).attr('data-character-set').split(',');
            var possible = '';
            if($.inArray('a-z', dataSet) >= 0){
                possible += 'abcdefghijklmnopqrstuvwxyz';
            }
            if($.inArray('A-Z', dataSet) >= 0){
                possible += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            }
            if($.inArray('0-9', dataSet) >= 0){
                possible += '0123456789';
            }
            if($.inArray('#', dataSet) >= 0){
                possible += '![]{}()%&*$#^<>~@|';
            }
            var text = '';
            for(var i=0; i < $(id).attr('data-size'); i++) {
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            return text;
        }

        // Create a new password on page load
        $('input[rel="gp"]').each(function(){
            $(this).val(randString($(this)));
        });

        // Create a new password
        $(".getNewPass").click(function(){
            var field = $(this).closest('div').find('input[rel="gp"]');
            field.val(randString(field));
        });

        // Auto Select Pass On Focus
        $('input[rel="gp"]').on("click", function () {
            $(this).select();
        });

        $(document).ready(function() {
            $('.js-example-theme-single').select2();
            $(document).on('change','#options',function () {
                $.ajax({
                    url: '{{route('ajax.structurestype')}}'+'/'+ $('#options').val(),
                    method: "GET",
                    success: function(response) {
                        // $('.select2-voitures').select2().destroy();
                        $(".structures").html(response);
                        $('.js-example-theme-single').select2();
                    }
                });
            });
        });
    </script>
@endsection
