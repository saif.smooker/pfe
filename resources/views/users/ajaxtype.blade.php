<label class="col-sm-2 col-form-label">Structure</label>
<div class="col-sm-10">
    <select name="structure" class="js-example-theme-single2 form-control">
        <option disabled selected >Choisir la structure pour l'utilisateur</option>
        @foreach($structures as $structure)
            <option value="{{$structure->id}}">{{$structure->nom}}</option>
        @endforeach
    </select>
</div>
