@extends('layout')
@section('css')
    <!-- Select 2 css -->
    <link rel="stylesheet" href="{{asset('bower_components/select2/css/select2.min.css')}}"/>
@endsection
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont icofont-user bg-c-blue"></i>
                    <div class="d-inline">
                        <h4>Utilisateurs</h4>
                        <span>Afficher un<code style="text-transform: uppercase">Utilisateur</code></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="/">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Afficher Un Utilisateur </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <!-- Basic Form Inputs card start -->
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">Informations</h4>
                        <form method="post" action="{{route('user.store')}}">
                            @csrf
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Nom</label>
                                <div class="col-sm-10">
                                    <p>{{$user->name}}
                                        @if($user->hasrole('admin'))<span class="badge badge-success">admin</span>@endif
                                        @if($user->hasrole('responsable'))<span class="badge badge-info">responsable</span>@endif
                                    </p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Email</label>
                                <div class="col-sm-10">
                                    <p>{{$user->email}}</p>
                                </div>
                            </div>
{{--                            <div class="form-group row">--}}
{{--                                <label class="col-sm-2 col-form-label">Password</label>--}}
{{--                                <div class="col-sm-10">--}}
{{--                                    <p>{{$user->password}}</p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Matricule</label>
                                <div class="col-sm-10">
                                    <p>{{$user->matricule}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label"> Structure</label>
                                <div class="col-sm-10">
                                    <p>{{$user->structure->type->name}}|{{$user->structure->nom}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label"> Demande Versement(s)</label>
                                <div class="col-sm-10">
                                    <p>{{$user->versements->count()}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label"> Demande Consultation(s)</label>
                                <div class="col-sm-10">
                                    <p>{{$user->consultations->count()}}</p>
                                </div>
                            </div>


                        </form>
                    </div>
                </div>
                <!-- Basic Form Inputs card end -->
            </div>
        </div>

    </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{asset('bower_components/select2/js/select2.full.min.js')}}"></script>
    {{--    <script type="text/javascript" src="{{asset('assets/pages/advance-elements/select2-custom.js')}}"></script>--}}
    <script>

        $(document).ready(function() {
            $('.js-example-theme-single').select2();
            $(document).on('change','#options',function () {
                $.ajax({
                    url: '{{route('ajax.structurestype')}}'+'/'+ $('#options').val(),
                    method: "GET",
                    success: function(response) {
                        // $('.select2-voitures').select2().destroy();
                        $(".structures").html(response);
                        $('.js-example-theme-single').select2();
                    }
                });
            });
        });
    </script>
@endsection
