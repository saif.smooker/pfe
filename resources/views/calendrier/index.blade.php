@extends('layout')
@section('nom')

@endsection
@section('css')
    <!-- Calender css -->
    <link rel="stylesheet" type="text/css" href="{{asset('bower_components/fullcalendar/css/fullcalendar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('bower_components/fullcalendar/css/fullcalendar.print.css')}}" media='print'>
@endsection
@section('content')

    <!-- Page-header start -->
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont icofont-calendar bg-c-pink"></i>
                    <div class="d-inline">
                        <h4>Full Calender</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="index.html">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Event Calendar</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Full Calender</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-header end -->
    <div class="page-body">
        <div class="card">
            <div class="card-header">
                <h5>Full Calender</h5>
                <div class="card-header-right">    <ul class="list-unstyled card-option">        <li><i class="icofont icofont-simple-left "></i></li>        <li><i class="icofont icofont-maximize full-card"></i></li>        <li><i class="icofont icofont-minus minimize-card"></i></li>        <li><i class="icofont icofont-refresh reload-card"></i></li>        <li><i class="icofont icofont-error close-card"></i></li>    </ul></div>
            </div>
            <div class="card-block">
                <div class="row">
                    <div class="col-xl-2 col-md-12">
                        <div id="external-events">
                            <h6 class="m-b-30 m-t-20">Events</h6>
                            <div class="fc-event ui-draggable ui-draggable-handle">My Event 1</div>
                            <div class="fc-event ui-draggable ui-draggable-handle">My Event 2</div>
                            <div class="fc-event ui-draggable ui-draggable-handle">My Event 3</div>
                            <div class="fc-event ui-draggable ui-draggable-handle">My Event 4</div>
                            <div class="fc-event ui-draggable ui-draggable-handle">My Event 5</div>
                            <div class="checkbox-fade fade-in-primary m-t-10">
                                <label>
                                    <input type="checkbox" value="">
                                    <span class="cr">
                                                                        <i class="cr-icon icofont icofont-ui-check txt-primary"></i>
                                                                    </span>
                                    <span>Remove After Drop</span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-10 col-md-12">
                        <div id='calendar'></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="page-error">
        <div class="card text-center">
            <div class="card-block">
                <div class="m-t-10">
                    <i class="icofont icofont-warning text-white bg-c-yellow"></i>
                    <h4 class="f-w-600 m-t-25">Not supported</h4>
                    <p class="text-muted m-b-0">Full Calender not supported in this device</p>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <!-- Menu-Search css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/pages/menu-search/css/component.css')}}">
    <!--classic JS-->
    <script type="text/javascript" src="{{asset('assets/js/classie.js')}}"></script>
    <!-- calender js -->
    <script type="text/javascript" src="{{asset('bower_components/moment/js/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('bower_components/fullcalendar/js/fullcalendar.min.js')}}"></script>
    <!-- Custom js -->
    <script type="text/javascript" src="{{asset('assets/pages/full-calender/calendar.js')}}"></script>
@endsection
