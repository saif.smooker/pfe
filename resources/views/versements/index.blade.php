@extends('layout')
@section('nom')

@endsection
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont
                    icofont-file-alt
                        bg-c-blue"></i>
                    <div class="d-inline">
                        <h4>Document <b class="text-muted">({{$text==1 ? '1ere age' : $text==2 ? '2éme age' : '3éme age'}})</b></h4>
                        <span>Traitement Des <code style="text-transform: uppercase">Documents({{$text==1 ? '1ere age' : $text==2 ? '2éme age' : '3éme age'}})</code></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="/">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Traiter les Documents(
                                {{$text==1 ? '1ere age' : $text==2 ? '2éme age' : '3éme age'}}
{{--                                @if($text==1)1ére--}}
{{--                                @elseif($text==2)2éme--}}
{{--                                @else($text==3)3éme@endif--}}
                                )</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <!-- DOM/Jquery table start -->
                <div class="card">
{{--                    <div class="card-header">--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-md-6">--}}
{{--                                 </div>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <a href="{{route('document.create')}}" type="button" id="addRow" class="btn btn-primary m-b-20 float-lg-right">+ Créer Un Nouveau Document--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <div class="card-block">

                        <div class="table-responsive dt-responsive">
                            <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                <thead>
                                <tr>
                                    <th>Numéro</th>
                                    <th>Nombre Pages</th>
                                    <th>Nombre Documents</th>
                                    <th>Chapitre Comptable</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($documents as $document)
                                    <tr>
                                        <td>{{$document->num}}</td>
                                        <td>{{$document->pages}}</td>
                                        <td>{{$document->nbrdoc}}</td>
                                        <td>{{$document->chapitre}}</td>
                                        <td>
                                            <a href="#" class="badge badge-md bg-success" data-toggle="modal"
                                               data-target="#prearchivage{{$document->id}}">{{$text==1 ? 'Préarchivage' : 'Archivage'}}</a>
                                            <a href="#" data-toggle="modal"
                                               data-target="#suppression{{$document->id}}" class="badge badge-md bg-danger">Suppression</a>

                                        </td>
                                    </tr>
                                    <div class="modal fade" id="prearchivage{{$document->id}}" tabindex="-1" role="dialog">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Confirmation {{$text==1 ? 'de Préarchivage' : $text==2 ? 'd\'archivage' : ''}}</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <h5>Etes vous sûr de vouloir {{$text==1 ? 'préarchiver': $text==2 ? 'archiver' : ''}} ce document ?</h5>
                                                </div>
                                                <div class="modal-footer">
                                                    <form method="post" action="{{route('versement.decision')}}">
                                                        @csrf
                                                        <input type="hidden" name="decision" value="1">
                                                        <input type="hidden" name="document_id" class="doc_id" value="{{$document->id}}">
                                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Non</button>
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light ">Oui</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal fade" id="suppression{{$document->id}}" tabindex="-1" role="dialog">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Confirmation de Suppression</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <h5>Etes vous sûr de vouloir Supprimer ce document {{$text==3 ? 'a tout jamais' : ''}}?</h5>
                                                </div>
                                                <div class="modal-footer">
                                                    <form method="post" action="{{route('versement.decision')}}">
                                                        @csrf
                                                        <input type="hidden" name="decision" value="0">
                                                        <input type="hidden" name="document_id" class="doc_id" value="{{$document->id}}">
                                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Non</button>
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light ">Oui</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Numéro</th>
                                    <th>Nombre Pages</th>
                                    <th>Nombre Documents</th>
                                    <th>Chapitre Comptable</th>
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- DOM/Jquery table end -->
            </div>
        </div>
    </div>

@endsection
@section('scripts')
{{--    <script>--}}
{{--        function ShowModalPrearchivage(documentid) {--}}
{{--            $('.doc_id').val(documentid);--}}
{{--            $('#prearchivage').modal('show');--}}
{{--        }--}}
{{--        function ShowModalSuppression(documentid) {--}}
{{--            $('.doc_id').val(documentid);--}}
{{--            $('#suppression').modal('show');--}}
{{--        }--}}
{{--    </script>--}}
@endsection
