@extends('layout')
@section('nom')

@endsection
@section('css')
    <link rel="stylesheet" href="{{asset('bower_components/datepicker/datepicker.min.css')}}" />
    <style>
        .modal{
            z-index:9999 !important;
        }
        .datepicker-container{
            z-index: 99999!important;
        }
    </style>
@endsection
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont
                    icofont-file-alt
                        bg-c-blue"></i>
                    <div class="d-inline">
                        <h4>Plans D'enlévements</h4>
                        {{--                        <span></code></span>--}}
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="/">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Plan d'enlevements</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <!-- DOM/Jquery table start -->
                <div class="card">
                    {{--                    <div class="card-header">--}}
                    {{--                        <div class="row">--}}
                    {{--                            <div class="col-md-6">--}}
                    {{--                                 </div>--}}

                    {{--                            <div class="col-md-6">--}}
                    {{--                                <a href="{{route('document.create')}}" type="button" id="addRow" class="btn btn-primary m-b-20 float-lg-right">+ Créer Un Nouveau Document--}}
                    {{--                                </a>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    <div class="card-block">

                        <div class="table-responsive dt-responsive">
                            <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                <thead>
                                <tr>
                                    <th>Utilisateur</th>
                                    <th>Structure</th>
                                    <th>Code</th>
                                    <th>Moyens Materiéls</th>
                                    <th>Moyens Humaines</th>
                                    <th>Nombre Documents</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($enlevements as $enlevement)
                                    <tr>
                                        <td>{{$enlevement->user->name}}</td>
                                        <td>{{$enlevement->user->structure->nom}}</td>
                                        <td>{{$enlevement->code}}</td>
                                        <td>{{$enlevement->materiel}}</td>
                                        <td>{{$enlevement->humaines}}</td>
                                        <td>{{$enlevement->documents->count()}}</td>
                                        <td>
                                            @if($enlevement->date)
                                            <a href="{{route('enlevement.calendar')}}" class="badge badge-md badge-success">calendrier</a>
                                            @else
                                            <a href="#"  data-toggle="modal"
                                               data-target="#suppression{{$enlevement->id}}" class="badge badge-md bg-info">Préciser une date</a>
                                            @endif
                                        </td>
                                    </tr>
                                    <div class="modal fade" id="suppression{{$enlevement->id}}" tabindex="-1" role="dialog">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">préciser une date d'enlevement</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="post" action="{{route('enlevement.date',$enlevement->id)}}">
                                                        @csrf
                                                        @method('put')
                                                        <label for="">date d'enlevement</label>
                                                        <input type="text" name="date" class="form-control datepicker">
                                                        <div class="modal-footer">
                                                            <input type="hidden" name="decision" value="1">
                                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Non</button>
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light ">Oui</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Utilisateur</th>
                                    <th>Structure</th>
                                    <th>Code</th>
                                    <th>Moyens Materiéls</th>
                                    <th>Moyens Humaines</th>
                                    <th>Nombre Documents</th>
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- DOM/Jquery table end -->
            </div>
        </div>
    </div>

    {{--    <div class="modal fade" id="suppression" tabindex="-1" role="dialog">--}}
    {{--        <div class="modal-dialog" role="document">--}}
    {{--            <div class="modal-content">--}}
    {{--                <div class="modal-header">--}}
    {{--                    <h4 class="modal-title">Confirmation de Suppression</h4>--}}
    {{--                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
    {{--                        <span aria-hidden="true">&times;</span>--}}
    {{--                    </button>--}}
    {{--                </div>--}}
    {{--                <div class="modal-body">--}}
    {{--                    <h5>Etes vous sûr de vouloir Supprimer ce document {{$text==3 ? 'a tout jamais' : ''}}?</h5>--}}
    {{--                </div>--}}
    {{--                <div class="modal-footer">--}}
    {{--                    <form method="post" action="{{route('versement.decision')}}">--}}
    {{--                        @csrf--}}
    {{--                        <input type="hidden" name="decision" value="0">--}}
    {{--                        <input type="hidden" name="document_id" class="doc_id" value="">--}}
    {{--                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Non</button>--}}
    {{--                        <button type="submit" class="btn btn-primary waves-effect waves-light ">Oui</button>--}}
    {{--                    </form>--}}
    {{--                </div>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--    </div>--}}
@endsection
@section('scripts')
    <script src="{{asset('bower_components/datepicker/datepicker.min.js')}}"></script>
    <script>
        $('.datepicker').datepicker({
            startDate:{{\Carbon\Carbon::now()->format('d/m/Y')}},
            language: 'fr-CA',
        });
        function ShowModalPrearchivage(documentid) {
            $('.doc_id').val(documentid);
            $('#prearchivage').modal('show');
        }
        function ShowModalSuppression(documentid) {
            $('.doc_id').val(documentid);
            $('#suppression').modal('show');
        }
    </script>
@endsection
