@extends('layout')
@section('nom')

@endsection
@section('css')
    {{--    <link rel="stylesheet" href="{{asset('bower_components/datepicker/datepicker.min.css')}}" />--}}
    <!-- Calender css -->
    <link rel="stylesheet" type="text/css" href="{{asset('bower_components/fullcalendar/css/fullcalendar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('bower_components/fullcalendar/css/fullcalendar.print.css')}}" media='print'>
@endsection
@section('content')
    <!-- Page-header start -->
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont icofont-calendar bg-c-pink"></i>
                    <div class="d-inline">
                        <h4>Full Calender</h4>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="index.html">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Event Calendar</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Full Calender</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Page-header end -->
    <div class="page-body">
        <div class="card">
            <div class="card-header">
                <h5>Calendrier d'enlévements</h5>
                <div class="card-header-right">    <ul class="list-unstyled card-option">        <li><i class="icofont icofont-simple-left "></i></li>        <li><i class="icofont icofont-maximize full-card"></i></li>        <li><i class="icofont icofont-minus minimize-card"></i></li>        <li><i class="icofont icofont-refresh reload-card"></i></li>        <li><i class="icofont icofont-error close-card"></i></li>    </ul></div>
            </div>
            <div class="card-block">
                <div class="row">
                    {{--                    <div class="col-xl-2 col-md-12">--}}
                    {{--                        <div id="external-events">--}}
                    {{--                            <h6 class="m-b-30 m-t-20">Events</h6>--}}
                    {{--                            <div class="fc-event ui-draggable ui-draggable-handle">My Event 1</div>--}}
                    {{--                            <div class="fc-event ui-draggable ui-draggable-handle">My Event 2</div>--}}
                    {{--                            <div class="fc-event ui-draggable ui-draggable-handle">My Event 3</div>--}}
                    {{--                            <div class="fc-event ui-draggable ui-draggable-handle">My Event 4</div>--}}
                    {{--                            <div class="fc-event ui-draggable ui-draggable-handle">My Event 5</div>--}}
                    {{--                            <div class="checkbox-fade fade-in-primary m-t-10">--}}
                    {{--                                <label>--}}
                    {{--                                    <input type="checkbox" value="">--}}
                    {{--                                    <span class="cr">--}}
                    {{--                                                                        <i class="cr-icon icofont icofont-ui-check txt-primary"></i>--}}
                    {{--                                                                    </span>--}}
                    {{--                                    <span>Remove After Drop</span>--}}
                    {{--                                </label>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    <div class="col-xl-12 col-md-12">
                        <div id='calendar'></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="fullCalModal" class="modal fade">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 id="modalTitle1" class="modal-title"></h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
                </div>
                <div id="modalBody1" class="modal-body">
                </div>
                <div class="modal-footer" id="modalFooter1">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{asset('bower_components/moment/js/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('bower_components/fullcalendar/js/fullcalendar.js')}}"></script>
    {{--    <script type="text/javascript" src="{{asset('assets/pages/full-calender/calendar.js')}}"></script>--}}
    {{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/locale-all.min.js" integrity="sha512-L0BJbEKoy0y4//RCPsfL3t/5Q/Ej5GJo8sx1sDr56XdI7UQMkpnXGYZ/CCmPTF+5YEJID78mRgdqRCo1GrdVKw==" crossorigin="anonymous"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/locale/fr.min.js" integrity="sha512-vz2hAYjYuxwqHQAgHPZvry+DTuwemFT/aBIDmgE0cnmYENu/+t8c3u/nX2Ont6e+3m+W6FEKxN1granjgGfr1Q==" crossorigin="anonymous"></script>

    <script>
        var curYear = moment().format('YYYY');
        var curMonth = moment().format('MM');
        var today = moment();
        $(document).ready(function() {
            // $('#external-events .fc-event').each(function() {
            //
            //     // store data so the calendar knows to render an event upon drop
            //     $(this).data('event', {
            //         title: $.trim($(this).text()), // use the element's text as the event title
            //         stick: true // maintain when user navigates (see docs on the renderEvent method)
            //     });
            //
            //     // make the event draggable using jQuery UI
            //     $(this).draggable({
            //         zIndex: 999,
            //         revert: true, // will cause the event to go back to its
            //         revertDuration: 0 //  original position after the drag
            //     });
            //
            // });
            // $.fullCalendar.locale('fr', {
            //     // strings we need that are neither in Moment nor datepicker
            //     "day": "Jour",
            //     "week": "Semaine",
            //     "month": "Mois",
            //     "list": "Mon planning"
            // }, {
            //     closeText: 'Fermer',
            //     prevText: 'Précédent',
            //     nextText: 'Suivant',
            //     currentText: 'Aujourd\'hui',
            //     dayNamesMin: ['D','L','M','M','J','V','S']
            // });

            $('#calendar').fullCalendar({
                locale:'fr',
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,listMonth',
                },
                buttonText: {
                    today:    'Aujourd\'hui',
                    month:    'Mois',
                    week:     'Semaine',
                    day:      'Jour',
                    list:     'Planning '
                },
                // datepickerLocale:'fr',
                defaultDate: '{{date('Y-m-d')}}',
                displayEventTime: false,
                navLinks: true, // can click day/week names to navigate views
                businessHours: true, // display business hours
                editable: false,
                droppable: false, // this allows things to be dropped onto the calendar
                // drop: function() {
                //
                //     // is the "remove after drop" checkbox checked?
                //     if ($('#checkbox2').is(':checked')) {
                //         // if so, remove the element from the "Draggable Events" list
                //         $(this).remove();
                //     }
                // },
                events: [
                        @foreach($enlevements as $enlevement)
                    {
                        id: '{{$enlevement->id}}',
                        title: '{{$enlevement->code}}',
                        start: '{{$enlevement->date}}',
                        end: '{{$enlevement->date}}',
                        borderColor: moment('{{$enlevement->date}}', 'YYYY-MM-DD') > today ? '#fd001d' : '#93BE52',
                        backgroundColor: moment('{{$enlevement->date}}', 'YYYY-MM-DD') > today ? '#fd001d' : '#93BE52',
                        textColor: '#fff',
                        materiel:'{{$enlevement->materiel}}',
                        humaines:'{{$enlevement->humaines}}',
                        structure:'{{$enlevement->structure->nom}}|{{$enlevement->structure->adresse}}'
                    },
                    @endforeach
                ],
                eventClick:  function(event, jsEvent, view) {
                    var modalfooter = $('#modalFooter1');
                    $('#modalTitle1').html(event.title);
                    $('#modalBody1').html('<div class=row"> <div class="col-md-6"><b>Structure</b></div> <div class="col-md-6">'+ event.structure +'</div> </div> <div class=row"> <div class="col-md-6"><b>moyens materiels</b></div> <div class="col-md-6">'+ event.materiel +'</div> </div> <div class=row"> <div class="col-md-6"><b>moyens humaines</b></div> <div class="col-md-6">'+ event.humaines +'</div> </div>');
                    $('#fullCalModal').modal();
                },
            });

        });
    </script>
@endsection
