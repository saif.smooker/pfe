@extends('layout')
@section('nom')

@endsection
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont
                    icofont-file-exe
                        bg-c-blue"></i>
                    <div class="d-inline">
                        <h4>Nomenclature</h4>
                        <span>Liste Des <code style="text-transform: uppercase">Nomenclatures</code></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="/">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Liste des Nomenclatures</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <!-- DOM/Jquery table start -->
                <div class="card">

                    <div class="card-block">
                        <a href="{{route('nomenclature.create')}}" type="button" id="addRow" class="btn btn-primary m-b-20 float-lg-right">+ Créer Une Nouvelle Nomenclature
                        </a>
                        <div class="table-responsive dt-responsive">
                            <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                <thead>
                                <tr>
                                    <th>code</th>
                                    <th>Désignation</th>
                                    <th>Conservation 1ére age</th>
                                    <th>Conservation 2éme age</th>
                                    <th>Conservation 3éme age</th>
{{--                                    <th>Nombre d'utilisation</th>--}}
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($nomenclatures as $nomenclature)
                                    <tr>
                                        <td>{{$nomenclature->code}}</td>
                                        <td>{{$nomenclature->designation}}</td>
                                        <td>Année:{{json_decode($nomenclature->age1)->year}},Mois:{{json_decode($nomenclature->age1)->month}},Jour:{{json_decode($nomenclature->age1)->day}}</td>
                                        <td>Année:{{json_decode($nomenclature->age2)->year}},Mois:{{json_decode($nomenclature->age2)->month}},Jour:{{json_decode($nomenclature->age2)->day}}</td>
                                        <td>Année:{{json_decode($nomenclature->age3)->year}},Mois:{{json_decode($nomenclature->age3)->month}},Jour:{{json_decode($nomenclature->age3)->day}}</td>
{{--                                        <td></td>--}}
                                        <td>
                                            <a href="{{route('nomenclature.show',$nomenclature->id)}}" class="badge badge-md bg-success"><i class="fa fa-eye"></i></a>
                                            <a href="{{route('nomenclature.edit',$nomenclature->id)}}" class="badge badge-md bg-info"><i class="fa fa-pencil"></i></a>
                                            <a href="#" data-toggle="modal" data-target="#delete{{$nomenclature->id}}" class="badge badge-md bg-danger"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                    <div class="modal fade" id="delete{{$nomenclature->id}}" tabindex="-1" role="dialog">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Confirmation de Suppression</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <h5>Etes vous sûr de vouloir Supprimer cette Nomenclature "{{$nomenclature->designation}}"?</h5>
                                                </div>
                                                <div class="modal-footer">
                                                    <form id="form"  method="post" action="{{route('nomenclature.destroy',$nomenclature->id)}}">
                                                        @csrf
                                                        @method('delete')
                                                        <button type="button" class="btn btn-info waves-effect " data-dismiss="modal">Non</button>
                                                        <button type="submit" class="btn btn-danger waves-effect waves-light ">Oui</button>

                                                    </form>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>code</th>
                                    <th>Désignation</th>
                                    <th>Conservation 1ére age</th>
                                    <th>Conservation 1ére age</th>
                                    <th>Conservation 1ére age</th>
{{--                                    <th>Nombre d'utilisation</th>--}}
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- DOM/Jquery table end -->
            </div>
        </div>
    </div>
@endsection
