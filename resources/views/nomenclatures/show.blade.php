@extends('layout')
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont
                    icofont-file-exe
                        bg-c-blue"></i>
                    <div class="d-inline">
                        <h4>Nomenclatures</h4>
                        <span>Afficher Une<code style="text-transform: uppercase">Nomenclature</code></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="/">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Afficher Une Nomenclature</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <!-- Basic Form Inputs card start -->
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">Informations sur la Nomenclature</h4>
                        <div >
                            @csrf
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Code</label>
                                <div class="col-sm-10">
                                    <p>{{$nomenclature->code}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Désignation</label>
                                <div class="col-sm-10">
                                    <p>{{$nomenclature->designation}}</p>
                                </div>
                            </div>
                            <h4 class="sub-title">Dates Conservations 1ére age</h4>

                            <div class="form-group row d-flex justify-content-start">
                                <label class="col-sm-1 col-form-label">Année</label>
                                <div class="col-sm-2">
                                    <p>{{json_decode($nomenclature->age1)->year}}</p>
                                </div>
                                <label class="col-sm-1 col-form-label">Mois</label>
                                <div class="col-sm-2">
                                    <p>{{json_decode($nomenclature->age1)->month}}</p>
                                </div>
                                <label class="col-sm-1 col-form-label">jour</label>
                                <div class="col-sm-2">
                                    <p>{{json_decode($nomenclature->age1)->day}}</p>
                                </div>
                            </div>
                            <h4 class="sub-title">Dates Conservations 2éme age</h4>
                            <div class="form-group row">

                                <label class="col-sm-1 col-form-label">Année</label>
                                <div class="col-sm-2">
                                    <p>{{json_decode($nomenclature->age2)->year}}</p>
                                </div>
                                <label class="col-sm-1 col-form-label">Mois</label>
                                <div class="col-sm-2">
                                    <p>{{json_decode($nomenclature->age2)->month}}</p>
                                </div>
                                <label class="col-sm-1 col-form-label">jour</label>
                                <div class="col-sm-2">
                                    <p>{{json_decode($nomenclature->age2)->day}}</p>
                                </div>
                            </div>
                            <h4 class="sub-title">Dates Conservations 3éme age</h4>
                            <div class="form-group row">

                                <label class="col-sm-1 col-form-label">Année</label>
                                <div class="col-sm-2">
                                    <p>{{json_decode($nomenclature->age3)->year}}</p>
                                </div>
                                <label class="col-sm-1 col-form-label">Mois</label>
                                <div class="col-sm-2">
                                    <p>{{json_decode($nomenclature->age3)->month}}</p>
                                </div>
                                <label class="col-sm-1 col-form-label">jour</label>
                                <div class="col-sm-2">
                                    <p>{{json_decode($nomenclature->age3)->day}}</p>
                                </div>
                            </div>
                            <div class="col-md-12 text-center">
                            <a href="{{route('nomenclature.edit',$nomenclature->id)}}" class="col-md-4 btn btn-out-dashed btn-success btn-square">Modifier</a>
                            <a href="{{route('nomenclature.index')}}" class="col-md-4 btn btn-out-dashed btn-info btn-square">Retour</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Basic Form Inputs card end -->
            </div>
        </div>

    </div>
@endsection
