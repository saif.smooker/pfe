@extends('layout')
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont
                    icofont-file-exe
                        bg-c-blue"></i>
                    <div class="d-inline">
                        <h4>Nomenclatures</h4>
                        <span>Créer Une<code style="text-transform: uppercase">Nomenclature</code></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="/">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Créer Une Nomenclature</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <!-- Basic Form Inputs card start -->
                <div class="card">
                    <div class="card-block">
                        <h4 class="sub-title">Formulaire de Modification</h4>
                        <form method="post" action="{{route('nomenclature.store')}}">
                            @csrf
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Code</label>
                                <div class="col-sm-10">
                                    <input name="code" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Désignation</label>
                                <div class="col-sm-10">
                                    <input name="designation" type="text" class="form-control" placeholder="Type your title in Placeholder">
                                </div>
                            </div>
                            <h4 class="sub-title">Dates Conservations 1ére age</h4>

                            <div class="form-group row d-flex justify-content-start">
                                <label class="col-sm-1 col-form-label">Année</label>
                                <div class="col-sm-2">
                                    <select name="annee1" class="form-control">
                                        @for($i=0;$i<5;$i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                <label class="col-sm-1 col-form-label">Mois</label>
                                <div class="col-sm-2">
                                    <select name="mois1" class="form-control">
                                        @for($i=0;$i<11;$i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                <label class="col-sm-1 col-form-label">jour</label>
                                <div class="col-sm-2">
                                    <select name="jour1" class="form-control">
                                        @for($i=0;$i<29;$i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <h4 class="sub-title">Dates Conservations 2éme age</h4>
                            <div class="form-group row">

                                <label class="col-sm-1 col-form-label">Année</label>
                                <div class="col-sm-2">
                                    <select name="annee2" class="form-control">
                                        @for($i=0;$i<5;$i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                <label class="col-sm-1 col-form-label">Mois</label>
                                <div class="col-sm-2">
                                    <select name="mois2" class="form-control">
                                        @for($i=0;$i<11;$i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                <label class="col-sm-1 col-form-label">jour</label>
                                <div class="col-sm-2">
                                    <select name="jour2" class="form-control">
                                        @for($i=0;$i<29;$i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <h4 class="sub-title">Dates Conservations 3éme age</h4>
                            <div class="form-group row">

                                <label class="col-sm-1 col-form-label">Année</label>
                                <div class="col-sm-2">
                                    <select name="annee3" class="form-control">
                                        @for($i=0;$i<5;$i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                <label class="col-sm-1 col-form-label">Mois</label>
                                <div class="col-sm-2">
                                    <select name="mois3" class="form-control">
                                        @for($i=0;$i<11;$i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                <label class="col-sm-1 col-form-label">jour</label>
                                <div class="col-sm-2">
                                    <select name="jour3" class="form-control">
                                        @for($i=0;$i<29;$i++)
                                            <option value="{{$i}}">{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="text-center">
                            <button type="submit" class="col-md-3 btn btn-out-dashed btn-success btn-square">Créer</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- Basic Form Inputs card end -->
            </div>
        </div>

    </div>
@endsection
