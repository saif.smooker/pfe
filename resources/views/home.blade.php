@extends('layout')

@section('content')

    <div class="page-body">
        <div class="row">
            @role('admin')
            <!-- card1 start -->
            <div class="col-md-6 col-xl-3">
                <div class="card widget-card-1">
                    <div class="card-block-small">
                        <i class="icofont icofont-bank bg-c-blue card1-icon"></i>
                        <span class="text-c-blue f-w-600">Agences</span>
                        <h4>{{count($agences)}}</h4>
                        <div>
                                                            <span class="f-left m-t-10 text-muted">
                                                                <i class="text-c-blue f-16 icofont icofont-warning m-r-10"></i>
                                                                        ≈20 utilisateurs/agence
                                                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- card1 end -->
            <!-- card1 start -->
            <div class="col-md-6 col-xl-3">
                <div class="card widget-card-1">
                    <div class="card-block-small">
                        <i class="icofont icofont-bank-alt bg-c-pink card1-icon"></i>
                        <span class="text-c-pink f-w-600">Dir.Regionales</span>
                        <h4>{{count($regionals)}}</h4>
                        <div>
                                                            <span class="f-left m-t-10 text-muted">
                                                                <i class="text-c-pink f-16 icofont icofont-calendar m-r-10"></i>
                                                                 ≈18 utilisateurs/dir.Reg
                                                                </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- card1 end -->
            <!-- card1 start -->
            <div class="col-md-6 col-xl-3">
                <div class="card widget-card-1">
                    <div class="card-block-small">
                        <i class="icofont icofont-building-alt bg-c-green card1-icon"></i>
                        <span class="text-c-green f-w-600">Dir.Centrales</span>
                        <h4>{{count($centrals)}}</h4>
                        <div>
                                                            <span class="f-left m-t-10 text-muted">
                                                                <i class="text-c-green f-16 icofont icofont-tag m-r-10"></i>
                                                                    ≈10 utilisateurs/dir.Reg
                                                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- card1 end -->
            <!-- card1 start -->
            <div class="col-md-6 col-xl-3">
                <div class="card widget-card-1">
                    <div class="card-block-small">
                        <i class="icofont icofont-files bg-c-yellow card1-icon"></i>
                        <span class="text-c-yellow f-w-600">Préarchives</span>
                        <h4>{{count($prearchives)}}</h4>
                        <div>
                                                            <span class="f-left m-t-10 text-muted">
                                                                <i class="text-c-yellow f-16 icofont icofont-refresh m-r-10"></i>
                                                                ≈3 utilisateurs/dir.Reg
                                                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- card1 end -->
            @endrole
            @role('responsable')
            <div class="col-sm-4">
                <div class="card bg-c-pink text-white widget-visitor-card">
                    <div class="card-block-small text-center">
                        <h2>{{$utilisateurs}}</h2>
                        <h6>Utilisateurs</h6>
                        <i class="ti-user"></i>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="card bg-c-blue text-white widget-visitor-card">
                    <div class="card-block-small text-center">
                        <h2>{{$documents}}</h2>
                        <h6>Documents</h6>
                        <h6>Documents Produits : {{$documentage}}</h6>
                        <i class="icofont icofont-paper"></i>
                    </div>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="card bg-c-yellow text-white widget-visitor-card">
                    <div class="card-block-small text-center">
                        <h6>Demande Versements: {{$versements}}</h6>
                        @if(((Auth::user()->structure->type->id)==4))
                            <h6>Demande Consultations: {{$consultations}}</h6>
                            <h6>Demande Enlevements: {{$enlevements}}</h6>
                        @elseif(((Auth::user()->structure->type->id)==5))
                            <h6>Demande Enlevements: {{$enlevements}}</h6>
                        @endif
                        <i class="icofont icofont-ui-alarm"></i>

                    </div>
                </div>
            </div>

            @endrole
            @role('employée')
            <div class="col-sm-4">
                <div class="card bg-c-yellow text-white widget-visitor-card">
                    <div class="card-block-small text-center">
                        <h2>{{$traitements}}</h2>
                        <h6>traitements</h6>
                        <i class="icofont icofont-ui-alarm"></i>

                    </div>
                </div>
            </div>
            @endrole
            <!-- Project overview End -->
        </div>
    </div>

    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <!-- DOM/Jquery table start -->
                <div class="card">

                    <div class="card-block">
                        <h4>Historiques </h4>
                        <div class="table-responsive dt-responsive">
                            <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                <thead>
                                <tr>
                                    <th>Structure</th>
                                    <th>Utilisateur</th>
                                    {{--                                    <th>Age</th>--}}
                                    <th>Actions</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($historiques as $historique)
                                    <tr>
                                        <td>{{$historique->structure->nom}}</td>
                                        <td>{{$historique->user->name}}
                                            <span class="badge badge-info">
                                                {{\App\User::find($historique->user->id)->getRoleNames()->first()}}
                                            </span>
                                        </td>
                                        {{--                                        <td>{{$historique->age}}</td>--}}
                                        <td>{{$historique->action}}</td>
                                        <td>{{$historique->created_at}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Structure</th>
                                    <th>Utilisateur</th>
                                    {{--                                    <th>Age</th>--}}
                                    <th>Actions</th>
                                    <th>Date</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- DOM/Jquery table end -->
            </div>
        </div>
    </div>

@endsection
