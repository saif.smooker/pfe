@extends('layout')
@section('nom')

@endsection
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont
                    icofont-file-alt
                        bg-c-blue"></i>
                    <div class="d-inline">
                        <h4>Document</h4>
                        <span>Liste Des <code style="text-transform: uppercase">Documents</code></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="/">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Liste des Documents</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <!-- DOM/Jquery table start -->
                <div class="card">

                    <div class="card-block">
                        @if((Auth::user()->structure->type->id=='1')||(Auth::user()->structure->type->id=='2')||(Auth::user()->structure->type->id=='3'))
                        <a href="{{route('document.create')}}" type="button" id="addRow" class="btn btn-primary m-b-20 float-lg-right">+ Créer Un Nouveau Document
                        </a>
                        @endif
                        <div class="table-responsive dt-responsive">
                            <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                <thead>
                                <tr>
                                    <th>Numéro</th>
                                    <th>Nombre Pages</th>
                                    <th>Nombre Documents</th>
                                    <th>Chapitre Comptable</th>
                                    @if((Auth::user()->structure->type->id=='1')||(Auth::user()->structure->type->id=='2')||(Auth::user()->structure->type->id=='3'))
                                    <th>Limite Conservation Age 1 </th>
                                    @elseif(Auth::user()->structure->type->id=='4')
                                    <th>Lieu Conservation Age 2</th>
                                    @elseif(Auth::user()->structure->type->id=='5')
                                    <th>Lieu Conservation Age 3</th>
                                    @endif
                                    <th>Nomenclature</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($documents as $document)
                                    <tr>
                                        <td>{{$document->num}}</td>
                                        <td>{{$document->pages}}</td>
                                        <td>{{$document->nbrdoc}}</td>
                                        <td>{{$document->chapitre}}</td>
                                        @if((Auth::user()->structure->type->id=='1')||(Auth::user()->structure->type->id=='2')||(Auth::user()->structure->type->id=='3'))
                                            <td data-toggle="tooltip" data-placement="left" data-original-title="{{$document->age1->format('d-m-y')}}">{{$document->age1->diffForHumans()}} </td>
                                        @elseif(Auth::user()->structure->type->id=='4')
                                            <td data-toggle="tooltip" data-placement="left" data-original-title="{{$document->age2->format('d-m-y')}}">{{$document->age2->diffForHumans()}} </td>
                                        @elseif(Auth::user()->structure->type->id=='5')
                                            <td data-toggle="tooltip" data-placement="left" data-original-title="{{$document->age3->format('d-m-y')}}">{{$document->age3->diffForHumans()}} </td>
                                        @endif
                                        <td>{{$document->nomenclature->designation}}</td>
                                        <td>
                                            <a href="{{route('document.show',$document->id)}}" class="badge badge-md bg-success"><i class="fa fa-eye"></i></a>
{{--                                            <a href="{{route('document.edit',$document->id)}}" class="badge badge-md bg-info"><i class="fa fa-pencil"></i></a>--}}
{{--                                            @if($document->time($document->id)==1)--}}
{{--                                                <a href="#" data-toggle="modal" data-target="#delete{{$document->id}}" class="badge badge-md bg-danger"><i class="fa fa-trash"></i></a>--}}
{{--                                            @endif--}}
                                        </td>
                                    </tr>
                                    <div class="modal fade" id="delete{{$document->id}}" tabindex="-1" role="dialog">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Confirmation de Suppression</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <h5>Etes vous sûr de vouloir Supprimer ce Document "{{$document->num}}"?</h5>
                                                </div>
                                                <div class="modal-footer">
                                                    <form id="form"  method="post" action="{{route('document.destroy',$document->id)}}">
                                                        @csrf
                                                        @method('delete')
                                                        <button type="button" class="btn btn-info waves-effect " data-dismiss="modal">Non</button>
                                                        <button type="submit" class="btn btn-danger waves-effect waves-light ">Oui</button>

                                                    </form>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Numéro</th>
                                    <th>Nombre Pages</th>
                                    <th>Nombre Documents</th>
                                    <th>Chapitre Comptable</th>
                                    @if((Auth::user()->structure->type->id=='1')||(Auth::user()->structure->type->id=='2')||(Auth::user()->structure->type->id=='3'))
                                        <th>Limite Conservation Age 1 </th>
                                    @elseif(Auth::user()->structure->type->id=='4')
                                        <th>Lieu Conservation Age 2</th>
                                    @elseif(Auth::user()->structure->type->id=='5')
                                        <th>Lieu Conservation Age 3</th>
                                    @endif
                                    <th>Nomenclature</th>
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- DOM/Jquery table end -->
            </div>
        </div>
    </div>
@endsection
