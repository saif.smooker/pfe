@extends('layout')
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont
                    icofont-file-alt
                        bg-c-blue"></i>
                    <div class="d-inline">
                        <h4>Document</h4>
                        <span>Créer Un<code style="text-transform: uppercase">Document</code></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="/">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Créer Un Document</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <!-- Basic Form Inputs card start -->
                <div class="card">

                    <div class="card-block">
                        <h4 class="sub-title">Formulaire de création</h4>
                        <form method="post" action="{{route('document.store')}}">
                            @csrf
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Numéro document</label>
                                <div class="col-sm-10">
                                    <input name="num" type="number" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Nombre pages</label>
                                <div class="col-sm-10">
                                    <input name="pages" type="number" class="form-control" placeholder="Nombre pages">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Nombre documents</label>
                                <div class="col-sm-10">
                                    <input name="nbrdoc" type="number" class="form-control" placeholder="Nombre documents">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Chapitre Comptable</label>
                                <div class="col-sm-10">
                                    <input name="chapitre" type="text" class="form-control" placeholder="Chapitre Comptable">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Nomenclature</label>
                                <div class="col-sm-10">
                                    <select required name="nomenclature" class="form-control">
                                        <option disabled selected >Choisir la nomenclature</option>
                                        @foreach($nomenclatures as $nomenclature)
                                            <option value="{{$nomenclature->id}}">{{$nomenclature->designation}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="text-center">
                            <button type="submit" class="col-md-3 btn btn-out-dashed btn-success btn-square">Créer</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- Basic Form Inputs card end -->
            </div>
        </div>

    </div>
@endsection
