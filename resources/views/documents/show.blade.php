@extends('layout')
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont
                    icofont-file-alt
                        bg-c-blue"></i>
                    <div class="d-inline">
                        <h4>Document</h4>
                        <span>Afficher Un<code style="text-transform: uppercase">Document</code></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="/">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Afficher Un Document</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <!-- Basic Form Inputs card start -->
                <div class="card">

                    <div class="card-block">
                        <h4 class="sub-title">Informations d'un document</h4>
                        <div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Numéro document</label>
                                <div class="col-sm-10">
                                    <p>{{$document->num}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Nombre pages</label>
                                <div class="col-sm-10">
                                    <p>{{$document->pages}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Nombre documents</label>
                                <div class="col-sm-10">
                                    <p>{{$document->nbrdoc}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Chapitre Comptable</label>
                                <div class="col-sm-10">
                                    <p>{{$document->chapitre}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Nomenclature</label>
                                <div class="col-sm-10">
                                    {{$document->nomenclature->designation}}
                                </div>
                            </div>
                            <div class="col-md-12 text-center">
                                <a href="{{route('document.edit',$document->id)}}" class="col-md-4 btn btn-out-dashed btn-success btn-square">Modifier</a>
                                <a href="{{route('document.index')}}" class="col-md-4 btn btn-out-dashed btn-info btn-square">Retour</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Basic Form Inputs card end -->
            </div>
        </div>

    </div>
@endsection
