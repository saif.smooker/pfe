@extends('layout')
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont
                    icofont-@if($type->id==1)bank-alt @elseif($type->id==2)institution @elseif($type->id==3)building-alt @elseif($type->id==4)files @elseif($type->id==5)archive @else @endif
                        bg-c-blue"></i>
                    <div class="d-inline">
                        <h4>{{$type->name}}</h4>
                        <span>Modifier <code style="text-transform: uppercase"> {{$type->name}}</code></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="/">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Modifier {{$type->name}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <!-- Basic Form Inputs card start -->
                <div class="card">

                    <div class="card-block">
                        <h4 class="sub-title">Formulaire de Modification</h4>
                        <form method="post" action="{{route('structure.update',['type_id' => $type->id, 'structure' => $structure->id])}}">
                            @csrf
                            @method('put')
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Libellé</label>
                                <div class="col-sm-10">
                                    <input name="nom" value="{{$structure->nom}}" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Code</label>
                                <div class="col-sm-10">
                                    <input name="code" value="{{$structure->code}}" type="text" class="form-control" placeholder="Type your title in Placeholder">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Région</label>
                                <div class="col-sm-10">
                                    <select name="region" class="form-control">
                                        @foreach($regions as $region)
                                            <option {{$region->id==$structure->region->id ? 'selected' : ''}} value="{{$region->id}}">{{$region->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Adresse</label>
                                <div class="col-sm-10">
                                    <input name="adresse" value="{{$structure->adresse}}" type="text" class="form-control" placeholder="Type your title in Placeholder">
                                </div>
                            </div>
                            @if($type->id!=4 && $type->id!=5)
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Lieu Conservation 1ére Age</label>
                                    <div class="col-sm-10">
                                        <select name="age1" class="form-control">
                                            <option value="Bureau">Bureau</option>

                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Lieu Conservation 2éme Age</label>
                                    <div class="col-sm-10">
                                        <select name="prearchive" class="form-control">
                                            @foreach($prearchives as $prearchive)
                                                <option {{$structure->age2==$prearchive->id ? 'selected' : ''}} value="{{$prearchive->id}}">{{$prearchive->nom}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
{{--                                <div class="form-group row">--}}
{{--                                    <label class="col-sm-2 col-form-label">Lieu Conservation 3éme Age</label>--}}
{{--                                    <div class="col-sm-10">--}}
{{--                                        <select name="archive" class="form-control">--}}
{{--                                            @foreach($archives as $archive)--}}
{{--                                                <option  {{$structure->age3==$archive->id ? 'selected' : ''}} value="{{$archive->id}}">{{$archive->nom}}</option>--}}
{{--                                            @endforeach--}}
{{--                                        </select>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                            @endif
                            @if($type->id==4)
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Lieu Conservation 3éme Age</label>
                                    <div class="col-sm-10">
                                        <select name="archive" class="form-control">
                                            @foreach($archives as $archive)
                                                <option {{$structure->age3==$archive->id ? 'selected' : ''}} value="{{$archive->id}}">{{$archive->nom}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif
                            <div class="text-center">
                                <button type="submit" class="col-md-3 btn btn-out-dashed btn-success btn-square">submit</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- Basic Form Inputs card end -->
            </div>
        </div>

    </div>
@endsection
