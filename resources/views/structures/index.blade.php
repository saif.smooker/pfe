@extends('layout')
@section('nom')

@endsection
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont
                    icofont-@if($type->id==1)bank-alt @elseif($type->id==2)institution @elseif($type->id==3)building-alt @elseif($type->id==4)files @elseif($type->id==5)archive @else @endif
                    bg-c-blue"></i>
                    <div class="d-inline">
                        <h4>{{$type->name}}</h4>
                        <span>Liste des <code style="text-transform: uppercase">{{$type->name}}s</code></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="/">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Liste des {{$type->name}}s</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <!-- DOM/Jquery table start -->
                <div class="card">

                    <div class="card-block">
                        <a href="{{route('structure.create',$type->id)}}" type="button" id="addRow" class="btn btn-primary m-b-20 float-lg-right">+ Créer {{$type->id==4||$type->id==5 ? 'un nouveau' : 'une nouvelle' }}
                            {{$type->name}}
                        </a>
                        <div class="table-responsive dt-responsive">
                            <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                <thead>
                                <tr>
                                    <th>Libellé</th>
                                    <th>Code</th>
                                    <th>Adresse</th>
                                    <th>Region</th>
                                    @if($type->id!=4 && $type->id!=5)
                                    <th>Lieu Conservation Age 1 </th>
                                    <th>Lieu Conservation Age 2</th>
                                    @endif
                                    @if($type->id==4)
                                    <th>Lieu Conservation Age 3</th>
                                    @endif
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($structures as $structure)
                                    <tr>
                                        <td>{{$structure->nom}}</td>
                                        <td>{{$structure->code}}</td>
                                        <td>{{$structure->adresse}}</td>
                                        <td>{{$structure->region->name}}</td>
                                        @if($type->id!=4 && $type->id!=5)
{{--                                            {{dd($structure->age2($structure->age2))}}--}}
                                        <td>{{$structure->age1}}</td>

                                        <td>{{$structure->age2($structure->age2)->type->name}}|{{$structure->age2($structure->age2)->adresse}}</td>
                                        @endif
                                        @if($type->id==4)
                                        <td>{{$structure->age3($structure->age3)->type->name}}|{{$structure->age3($structure->age3)->adresse}}</td>
                                        @endif
                                        <td>
                                            <a href="{{route('structure.show',['type_id' => $type->id, 'structure' => $structure->id])}}" class="badge badge-md bg-success"><i class="fa fa-eye"></i></a>
                                            <a href="{{route('structure.edit',['type_id' => $type->id, 'structure' => $structure->id])}}" class="badge badge-md bg-info"><i class="fa fa-pencil"></i></a>
                                            <a href="#" data-toggle="modal" data-target="#delete{{$structure->id}}" class="badge badge-md bg-danger"><i class="fa fa-trash"></i></a>

                                        </td>
                                    </tr>
                                    <div class="modal fade" id="delete{{$structure->id}}" tabindex="-1" role="dialog">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Confirmation de Suppression</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <h5>Etes vous sûr de vouloir Supprimer cette {{$structure->type->name}} "{{$structure->nom}}"?</h5>
                                                </div>
                                                <div class="modal-footer">
                                                    <form id="form"  method="post" action="{{route('structure.delete',['type_id' => $type->id, 'structure' => $structure->id])}}">
                                                        @csrf
                                                        @method('delete')
                                                        <button type="button" class="btn btn-info waves-effect " data-dismiss="modal">Non</button>
                                                        <button type="submit" class="btn btn-danger waves-effect waves-light ">Oui</button>

                                                    </form>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Libellé</th>
                                    <th>Code</th>
                                    <th>Adresse</th>
                                    <th>Region</th>
                                    @if($type->id!=4 && $type->id!=5)
                                    <th>Lieu Conservation Age 1 </th>
                                    <th>Lieu Conservation Age 2</th>
                                    @endif
                                    @if($type->id==4)
                                    <th>Lieu Conservation Age 3</th>
                                    @endif
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- DOM/Jquery table end -->
            </div>
        </div>
    </div>
@endsection
