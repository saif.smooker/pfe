@extends('layout')
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont
                    icofont-@if($type->id==1)bank-alt @elseif($type->id==2)institution @elseif($type->id==3)building-alt @elseif($type->id==4)files @elseif($type->id==5)archive @else @endif
                        bg-c-blue"></i>
                    <div class="d-inline">
                        <h4>{{$type->name}}</h4>
                        <span>Afficher <code style="text-transform: uppercase">@if($type->id==2)Une Direction @endif
                                @if($type->id==3)Une Structure @endif
                                @if($type->id==4||$type->id==5)Un Centre de @endif {{$type->name}}</code></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="/">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Afficher @if($type->id==2)Une Direction @endif
                                @if($type->id==3)Une Structure @endif
                                @if($type->id==4||$type->id==5)Un Centre de @endif {{$type->name}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <!-- Basic Form Inputs card start -->
                <div class="card">

                    <div class="card-block">
                        <h4 class="sub-title">Information sur </h4>
                        <div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Libellé</label>
                                <div class="col-sm-10">
                                    <p>{{$structure->nom}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Code</label>
                                <div class="col-sm-10">
                                    <p>{{$structure->code}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Région</label>
                                <div class="col-sm-10">
                                    <p>{{$structure->region->name}}</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Adresse</label>
                                <div class="col-sm-10">
                                    <p>{{$structure->adresse}}</p>
                                </div>
                            </div>
                            @if($structure->type_id!=4 && $structure->type_id!=5)
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Lieu Conservation 1ére Age</label>
                                <div class="col-sm-10">
                                        <p>Bureau</p>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Lieu Conservation 2éme Age</label>
                                <div class="col-sm-10">
                                   <p>{{$structure->age2($structure->age2)->nom}}</p>
                                </div>
                            </div>
                            @endif
                            <div class="col-md-12 text-center">
                                <a href="{{route('structure.edit',['type_id' => $type->id, 'structure' => $structure->id])}}" class="col-md-4 btn btn-out-dashed btn-success btn-square">Modifier</a>
                                <a href="{{route('structure.index',['type_id' => $type->id, 'structure' => $structure->id])}}" class="col-md-4 btn btn-out-dashed btn-info btn-square">Retour</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Basic Form Inputs card end -->
            </div>
        </div>

    </div>
@endsection
