@extends('layout')
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont
                    icofont-@if($type->id==1)bank-alt @elseif($type->id==2)institution @elseif($type->id==3)building-alt @elseif($type->id==4)files @elseif($type->id==5)archive @else @endif
                        bg-c-blue"></i>
                    <div class="d-inline">
                        <h4>{{$type->name}}</h4>
                        <span>Créer <code style="text-transform: uppercase">Une {{$type->name}}</code></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="/">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Créer @if($type->id==2)Une Direction @endif
                                @if($type->id==3)Une Structure @endif
                                @if($type->id==4||$type->id==5)Un Centre de @endif {{$type->name}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <!-- Basic Form Inputs card start -->
                <div class="card">

                    <div class="card-block">
                        <h4 class="sub-title">Formulaire de Création</h4>
                        <form method="post" action="{{route('structure.store',$type_id)}}">
                            @csrf
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Libellé</label>
                                <div class="col-sm-10">
                                    <input name="nom" type="text" class="form-control" placeholder="Libellé">
                                </div>
                            </div>
{{--                            <div class="form-group row">--}}
{{--                                <label class="col-sm-2 col-form-label">Code</label>--}}
{{--                                <div class="col-sm-10">--}}
{{--                                    <input name="code" type="text" class="form-control" placeholder="">--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Code</label>
                                <div class="input-group col-sm-10">
                                    <input name="code" placeholder="Matricule Utilisateur" type="text" class="form-control" rel="gp" data-size="6" data-character-set="a-z,A-Z,0-9">
                                    <span class="input-group-btn"><button type="button" class="btn btn-default getNewPass"><span class="fa fa-refresh"></span></button></span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Région</label>
                                <div class="col-sm-10">
                                    <select name="region" class="form-control">
                                        @foreach($regions as $region)
                                            <option value="{{$region->id}}">{{$region->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Adresse</label>
                                <div class="col-sm-10">
                                    <input name="adresse" type="text" class="form-control" placeholder="Adresse">
                                </div>
                            </div>
                           @if($type_id!=4 && $type_id!=5)
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Lieu Conservation 1ére Age</label>
                                <div class="col-sm-10">
                                    <select name="age1" class="form-control">
                                        <option value="opt2">Bureau</option>

                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Lieu Conservation 2éme Age</label>
                                <div class="col-sm-10">
                                    <select name="prearchive" class="form-control">
                                     @foreach($prearchives as $prearchive)
                                        <option value="{{$prearchive->id}}">{{$prearchive->nom}}</option>
                                     @endforeach
                                    </select>
                                </div>
                            </div>

                            @endif
                            @if($type_id==4)
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Lieu Conservation 3éme Age</label>
                                <div class="col-sm-10">
                                    <select name="archive" class="form-control">
                                        @foreach($archives as $archive)
                                            <option value="{{$archive->id}}">{{$archive->nom}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @endif
                            <div class="text-center">
                            <button type="submit" class="col-md-3 btn btn-out-dashed btn-success btn-square">Créer</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- Basic Form Inputs card end -->
            </div>
        </div>

    </div>
@endsection
@section('scripts')
    <script>
        // Generate a password string
        function randString(id){
            var dataSet = $(id).attr('data-character-set').split(',');
            var possible = '';
            if($.inArray('a-z', dataSet) >= 0){
                possible += 'abcdefghijklmnopqrstuvwxyz';
            }
            if($.inArray('A-Z', dataSet) >= 0){
                possible += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            }
            if($.inArray('0-9', dataSet) >= 0){
                possible += '0123456789';
            }
            if($.inArray('#', dataSet) >= 0){
                possible += '![]{}()%&*$#^<>~@|';
            }
            var text = '';
            for(var i=0; i < $(id).attr('data-size'); i++) {
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            return text;
        }

        // Create a new password on page load
        $('input[rel="gp"]').each(function(){
            $(this).val(randString($(this)));
        });

        // Create a new password
        $(".getNewPass").click(function(){
            var field = $(this).closest('div').find('input[rel="gp"]');
            field.val(randString(field));
        });

        // Auto Select Pass On Focus
        $('input[rel="gp"]').on("click", function () {
            $(this).select();
        });
    </script>
@endsection
