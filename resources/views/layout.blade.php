<!DOCTYPE html>
<html lang="en">

<head>
    <title>GURU Able - Premium Admin Template </title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="description" content="#">
    <meta name="keywords"
          content="flat ui, admin Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">
    <!-- Google font--><link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="{{asset('bower_components/bootstrap/css/bootstrap.min.css')}}">
    <!-- themify-icons line icon -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/icon/themify-icons/themify-icons.css')}}">
    <!-- ico font -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/icon/icofont/css/icofont.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/icon/font-awesome/css/font-awesome.css')}}">
    <!-- flag icon framework css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/pages/flag-icon/flag-icon.min.css')}}">
    <!-- Menu-Search css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/pages/menu-search/css/component.css')}}">
    <!-- Data Table Css -->
    <link rel="stylesheet" type="text/css" href="{{asset('bower_components/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('bower_components/i18next')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/pages/data-table/css/buttons.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('bower_components/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}">
    @yield('css')
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('bower_components/iziToast/iziToast.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/jquery.mCustomScrollbar.css')}}">
</head>

<body>
<!-- Pre-loader start -->
<div class="theme-loader">
    <div class="ball-scale">
        <div class='contain'>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
            <div class="ring"><div class="frame"></div></div>
        </div>
    </div>
</div>
<!-- Pre-loader end -->
<div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">

        <nav class="navbar header-navbar pcoded-header">
            <div class="navbar-wrapper">

                <div class="navbar-logo">
                    <a class="mobile-menu" id="mobile-collapse" href="#!">
                        <i class="ti-menu"></i>
                    </a>
                    <a class="mobile-search morphsearch-search" href="#">
                        <i class="ti-search"></i>
                    </a>
                    <a href="{{route('home')}}">
                        <img class="img-fluid" src="{{asset('assets/images/logo.png')}}" alt="Theme-Logo" />
                    </a>
                    <a class="mobile-options">
                        <i class="ti-more"></i>
                    </a>
                </div>

                <div class="navbar-container container-fluid">
                    <ul class="nav-left">
                        <li>
                            <div class="sidebar_toggle"><a href="javascript:void(0)"><i class="ti-menu"></i></a></div>
                        </li>
{{--                        <li>--}}
{{--                            <a class="main-search morphsearch-search" href="#">--}}
{{--                                <!-- themify icon -->--}}
{{--                                <i class="ti-search"></i>--}}
{{--                            </a>--}}
{{--                        </li>--}}
                        <li>
                            <a href="#!" onclick="javascript:toggleFullScreen()">
                                <i class="ti-fullscreen"></i>
                            </a>
                        </li>
                    </ul>
                    <ul class="nav-right">
                        <li class="header-notification lng-dropdown">
                            <a href="#" id="dropdown-active-item">
                                <i class="flag-icon flag-icon-fr m-r-5"></i> Français
                            </a>

                        </li>

                        <li class="user-profile header-notification">
                            <a href="#!">
                                <img src="{{asset('assets/images/avatar-4.png')}}" class="img-radius" alt="User-Profile-Image">
                                <span> @yield('nom') {{Auth::user()->name}}</span>
                                <i class="ti-angle-down"></i>
                            </a>
                            <ul class="show-notification profile-notification">
{{--                                <li>--}}
{{--                                    <a href="{{route('user.edit',Auth::user()->id)}}">--}}
{{--                                        <i class="ti-settings"></i> Paramétres--}}
{{--                                    </a>--}}
{{--                                </li>--}}
                                <li>
                                    <a href="{{route('user.show',Auth::user()->id)}}">
                                        <i class="ti-user"></i> Profile
                                    </a>
                                </li>
{{--                                <li>--}}
{{--                                    <a href="email-inbox.html">--}}
{{--                                        <i class="ti-email"></i> My Messages--}}
{{--                                    </a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <a href="auth-lock-screen.html">--}}
{{--                                        <i class="ti-lock"></i> Lock Screen--}}
{{--                                    </a>--}}
{{--                                </li>--}}
                                <li>
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="ti-layout-sidebar-left"></i> Se Déconnecter
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <!-- search -->
                    <div id="morphsearch" class="morphsearch">
                        <form class="morphsearch-form">
                            <input class="morphsearch-input" type="search" placeholder="Search..." />
                            <button class="morphsearch-submit" type="submit">Search</button>
                        </form>
                        <div class="morphsearch-content">
                            <div class="dummy-column">
                                <h2>People</h2>
                                <a class="dummy-media-object" href="#!">
                                    <img src="{{asset('assets/images/avatar-1.jpg')}}" alt="Sara Soueidan" />
                                    <h3>Sara Soueidan</h3>
                                </a>
                                <a class="dummy-media-object" href="#!">
                                    <img src="{{asset('assets/images/avatar-2.jpg')}}" alt="Shaun Dona" />
                                    <h3>Shaun Dona</h3>
                                </a>
                            </div>
                            <div class="dummy-column">
                                <h2>Popular</h2>
                                <a class="dummy-media-object" href="#!">
                                    <img src="{{asset('assets/images/avatar-3.jpg')}}" alt="PagePreloadingEffect" />
                                    <h3>Page Preloading Effect</h3>
                                </a>
                                <a class="dummy-media-object" href="#!">
                                    <img src="{{asset('assets/images/avatar-4.png')}}" alt="DraggableDualViewSlideshow" />
                                    <h3>Draggable Dual-View Slideshow</h3>
                                </a>
                            </div>
                            <div class="dummy-column">
                                <h2>Recent</h2>
                                <a class="dummy-media-object" href="#!">
                                    <img src="{{asset('assets/images/avatar-5.jpg')}}" alt="TooltipStylesInspiration" />
                                    <h3>Tooltip Styles Inspiration</h3>
                                </a>
                                <a class="dummy-media-object" href="#!">
                                    <img src="{{asset('assets/images/avatar-6.jpg')}}" alt="NotificationStyles" />
                                    <h3>Notification Styles Inspiration</h3>
                                </a>
                            </div>
                        </div>
                        <!-- /morphsearch-content -->
                        <span class="morphsearch-close"><i class="icofont icofont-search-alt-1"></i></span>
                    </div>
                    <!-- search end -->
                </div>
            </div>
        </nav>

        <!-- Sidebar chat start -->
        <div id="sidebar" class="users p-chat-user showChat">
            <div class="had-container">
                <div class="card card_main p-fixed users-main">
                    <div class="user-box">
                        <div class="card-block">
                            <div class="right-icon-control">
                                <input type="text" class="form-control  search-text" placeholder="Search Friend" id="search-friends">
                                <div class="form-icon">
                                    <i class="icofont icofont-search"></i>
                                </div>
                            </div>
                        </div>
                        <div class="main-friend-list">
                            <div class="media userlist-box" data-id="1" data-status="online" data-username="Josephin Doe" data-toggle="tooltip" data-placement="left" title="Josephin Doe">
                                <a class="media-left" href="#!">
                                    <img class="media-object" src="{{asset('assets/images/avatar-1.jpg')}}" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Josephin Doe</div>
                                </div>
                            </div>
                            <div class="media userlist-box" data-id="2" data-status="online" data-username="Lary Doe" data-toggle="tooltip" data-placement="left" title="Lary Doe">
                                <a class="media-left" href="#!">
                                    <img class="media-object" src="{{asset('assets/images/avatar-2.jpg')}}" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Lary Doe</div>
                                </div>
                            </div>
                            <div class="media userlist-box" data-id="3" data-status="online" data-username="Alice" data-toggle="tooltip" data-placement="left" title="Alice">
                                <a class="media-left" href="#!">
                                    <img class="media-object" src="{{asset('assets/images/avatar-3.jpg')}}" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Alice</div>
                                </div>
                            </div>
                            <div class="media userlist-box" data-id="4" data-status="online" data-username="Alia" data-toggle="tooltip" data-placement="left" title="Alia">
                                <a class="media-left" href="#!">
                                    <img class="media-object" src="{{asset('assets/images/avatar-4.png')}}" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Alia</div>
                                </div>
                            </div>
                            <div class="media userlist-box" data-id="5" data-status="online" data-username="Suzen" data-toggle="tooltip" data-placement="left" title="Suzen">
                                <a class="media-left" href="#!">
                                    <img class="media-object" src="{{asset('assets/images/avatar-5.jpg')}}" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Suzen</div>
                                </div>
                            </div>
                            <div class="media userlist-box" data-id="6" data-status="offline" data-username="Michael Scofield" data-toggle="tooltip" data-placement="left" title="Michael Scofield">
                                <a class="media-left" href="#!">
                                    <img class="media-object" src="{{asset('assets/images/avatar-6.jpg')}}" alt="Generic placeholder image">
                                    <div class="live-status bg-danger"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Michael Scofield</div>
                                </div>
                            </div>
                            <div class="media userlist-box" data-id="7" data-status="online" data-username="Irina Shayk" data-toggle="tooltip" data-placement="left" title="Irina Shayk">
                                <a class="media-left" href="#!">
                                    <img class="media-object" src="{{asset('assets/images/avatar-7.jpg')}}" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Irina Shayk</div>
                                </div>
                            </div>
                            <div class="media userlist-box" data-id="8" data-status="offline" data-username="Sara Tancredi" data-toggle="tooltip" data-placement="left" title="Sara Tancredi">
                                <a class="media-left" href="#!">
                                    <img class="media-object" src="{{asset('assets/images/avatar-1.jpg')}}" alt="Generic placeholder image">
                                    <div class="live-status bg-danger"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Sara Tancredi</div>
                                </div>
                            </div>
                            <div class="media userlist-box" data-id="9" data-status="online" data-username="Samon" data-toggle="tooltip" data-placement="left" title="Samon">
                                <a class="media-left" href="#!">
                                    <img class="media-object" src="{{asset('assets/images/avatar-2.jpg')}}" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Samon</div>
                                </div>
                            </div>
                            <div class="media userlist-box" data-id="10" data-status="online" data-username="Daizy Mendize" data-toggle="tooltip" data-placement="left" title="Daizy Mendize">
                                <a class="media-left" href="#!">
                                    <img class="media-object" src="{{asset('assets/images/avatar-3.jpg')}}" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Daizy Mendize</div>
                                </div>
                            </div>
                            <div class="media userlist-box" data-id="11" data-status="offline" data-username="Loren Scofield" data-toggle="tooltip" data-placement="left" title="Loren Scofield">
                                <a class="media-left" href="#!">
                                    <img class="media-object" src="{{asset('assets/images/avatar-4.png')}}" alt="Generic placeholder image">
                                    <div class="live-status bg-danger"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Loren Scofield</div>
                                </div>
                            </div>
                            <div class="media userlist-box" data-id="12" data-status="online" data-username="Shayk" data-toggle="tooltip" data-placement="left" title="Shayk">
                                <a class="media-left" href="#!">
                                    <img class="media-object" src="{{asset('assets/images/avatar-5.jpg')}}" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Shayk</div>
                                </div>
                            </div>
                            <div class="media userlist-box" data-id="13" data-status="offline" data-username="Sara" data-toggle="tooltip" data-placement="left" title="Sara">
                                <a class="media-left" href="#!">
                                    <img class="media-object" src="{{asset('assets/images/avatar-6.jpg')}}" alt="Generic placeholder image">
                                    <div class="live-status bg-danger"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Sara</div>
                                </div>
                            </div>
                            <div class="media userlist-box" data-id="14" data-status="online" data-username="Doe" data-toggle="tooltip" data-placement="left" title="Doe">
                                <a class="media-left" href="#!">
                                    <img class="media-object" src="{{asset('assets/images/avatar-7.jpg')}}" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Doe</div>
                                </div>
                            </div>
                            <div class="media userlist-box" data-id="15" data-status="online" data-username="Lary" data-toggle="tooltip" data-placement="left" title="Lary">
                                <a class="media-left" href="#!">
                                    <img class="media-object" src="{{asset('assets/images/avatar-1.jpg')}}" alt="Generic placeholder image">
                                    <div class="live-status bg-success"></div>
                                </a>
                                <div class="media-body">
                                    <div class="f-13 chat-header">Lary</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Sidebar inner chat start-->
        <div class="showChat_inner">
            <div class="media chat-inner-header">
                <a class="back_chatBox">
                    <i class="icofont icofont-rounded-left"></i> Josephin Doe
                </a>
            </div>
            <div class="media chat-messages">
                <a class="media-left photo-table" href="#!">
                    <img class="media-object img-circle m-t-5" src="{{asset('assets/images/avatar-1.jpg')}}" alt="Generic placeholder image">
                </a>
                <div class="media-body chat-menu-content">
                    <div class="">
                        <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                        <p class="chat-time">8:20 a.m.</p>
                    </div>
                </div>
            </div>
            <div class="media chat-messages">
                <div class="media-body chat-menu-reply">
                    <div class="">
                        <p class="chat-cont">I'm just looking around. Will you tell me something about yourself?</p>
                        <p class="chat-time">8:20 a.m.</p>
                    </div>
                </div>
                <div class="media-right photo-table">
                    <a href="#!">
                        <img class="media-object img-circle m-t-5" src="{{asset('assets/images/user.png')}}" alt="Generic placeholder image">
                    </a>
                </div>
            </div>
            <div class="chat-reply-box p-b-20">
                <div class="right-icon-control">
                    <input type="text" class="form-control search-text" placeholder="Share Your Thoughts">
                    <div class="form-icon">
                        <i class="icofont icofont-paper-plane"></i>
                    </div>
                </div>
            </div>
        </div>
        <!-- Sidebar inner chat end-->
        <div class="pcoded-main-container">
            <div class="pcoded-wrapper">
                <nav class="pcoded-navbar">
                    <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a></div>
                    <div class="pcoded-inner-navbar main-menu">
                        <div class="">
                            <div class="main-menu-header">
                                <img class="img-40 img-radius" src="{{asset('assets/images/avatar-4.png')}}" alt="User-Profile-Image">
                                <div class="user-details">
                                    <span>{{Auth::user()->name}}</span>
                                    <span id="more-details">
                                        @role('admin') admin @endrole
                                        @role('responsable') responsable @endrole
                                        @role('employée') employée @endrole {{Auth::user()->structure->type->name ? Auth::user()->structure->type->name : ''}}
                                        <i class="ti-angle-down"></i>
                                    </span>
                                </div>
                            </div>

                            <div class="main-menu-content">
                                <ul>
                                    <li class="more-details">
                                        <a href="user-profile.html"><i class="ti-user"></i>View Profile</a>
                                        <a href="#!"><i class="ti-settings"></i>Settings</a>
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="ti-layout-sidebar-left"></i> Se Déconnecter
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </div>
                        </div>
{{--                        <div class="pcoded-search">--}}
{{--                            <span class="searchbar-toggle">  </span>--}}
{{--                            <div class="pcoded-search-box ">--}}
{{--                                <input type="text" placeholder="Rechercher">--}}
{{--                                <span class="search-icon"><i class="ti-search" aria-hidden="true"></i></span>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div class="pcoded-navigatio-lavel" data-i18n="nav.category.navigation">Navigation</div>
                        <ul class="pcoded-item pcoded-left-item">
                            <li class="">
                                <a href="{{route('home')}}">
                                    <span class="pcoded-micon"><i class="ti-home"></i><b>D</b></span>
                                    <span class="pcoded-mtext" data-i18n="nav.dash.main">Dashboard</span>
                                    <span class="pcoded-mcaret"></span>
                                </a>
                            </li>
                            @role('admin')
                            <li class="pcoded-hasmenu">
                                <a href="javascript:void(0)">
                                    <span class="pcoded-micon"><i class="ti-user"></i><b>P</b></span>
                                    <span class="pcoded-mtext" data-i18n="nav.page_layout.main">Utilisateurs</span>
{{--                                    {{--                                    <span class="pcoded-badge label label-warning">NEW</span>--}}
                                    <span class="pcoded-mcaret"></span>
                                </a>
                                <ul class="pcoded-submenu">
                                    <li class=" ">
                                        <a href="{{route('user.index')}}">
                                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                                            <span class="pcoded-mtext">Liste des Utilisateurs</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                     <li class=" ">
                                                <a href="{{route('user.create')}}">
                                                    <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                                    <span class="pcoded-mtext" data-i18n="nav.page_layout.vertical.static-layout">Ajouter un utilisateur</span>
                                                    <span class="pcoded-mcaret"></span>
                                                </a>
                                     </li>
                                </ul>
                            </li>
                            <li class="pcoded-hasmenu">
                                <a href="javascript:void(0)">
                                    <span class="pcoded-micon"><i class="fa fa-building"></i><b>P</b></span>
                                    <span class="pcoded-mtext" data-i18n="nav.page_layout.main">Structures Centales</span>
                                    {{--                                    <span class="pcoded-badge label label-warning">NEW</span>--}}
                                    <span class="pcoded-mcaret"></span>
                                </a>
                                <ul class="pcoded-submenu">
                                    <li class=" ">
                                        <a href="{{route('structure.index','3')}}">
                                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                                            <span class="pcoded-mtext">Liste des Structures Centales</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                     <li class=" ">
                                                <a href="{{route('structure.create','3')}}">
                                                    <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                                    <span class="pcoded-mtext" data-i18n="nav.page_layout.vertical.static-layout">Ajouter une Structures Centales</span>
                                                    <span class="pcoded-mcaret"></span>
                                                </a>
                                     </li>
                                </ul>
                            </li>
                            <li class="pcoded-hasmenu">
                                <a href="javascript:void(0)">
                                    <span class="pcoded-micon"><i class="fa fa-institution"></i><b>P</b></span>
                                    <span class="pcoded-mtext" data-i18n="nav.page_layout.main">Structures Regionales</span>
                                    {{--                                    <span class="pcoded-badge label label-warning">NEW</span>--}}
                                    <span class="pcoded-mcaret"></span>
                                </a>
                                <ul class="pcoded-submenu">

                                    <li class=" pcoded-hasmenu">
                                        <a href="javascript:void(0)">
                                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                                            <span class="pcoded-mtext">Direction Regionales</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                        <ul class="pcoded-submenu">
                                            <li class=" ">
                                                <a href="{{route('structure.index','2')}}">
                                                    <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                                    <span class="pcoded-mtext" data-i18n="nav.page_layout.vertical.static-layout">Liste des Direction Regionales </span>
                                                    <span class="pcoded-mcaret"></span>
                                                </a>
                                            </li>
                                            <li class=" ">
                                                <a href="{{route('structure.create','2')}}">
                                                    <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                                    <span class="pcoded-mtext" data-i18n="nav.page_layout.vertical.header-fixed">Ajouter une Direction Regionales</span>
                                                    <span class="pcoded-mcaret"></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class=" pcoded-hasmenu">
                                        <a href="javascript:void(0)">
                                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                                            <span class="pcoded-mtext" data-i18n="nav.page_layout.horizontal.main">Agences</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                        <ul class="pcoded-submenu">
                                            <li class=" ">
                                                <a href="{{route('structure.index','1')}}" >
                                                    <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                                    <span class="pcoded-mtext" data-i18n="nav.page_layout.horizontal.static-layout">Liste des Agences</span>
                                                    <span class="pcoded-mcaret"></span>
                                                </a>
                                            </li>
                                            <li class=" ">
                                                <a href="{{route('structure.create','1')}}">
                                                    <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                                    <span class="pcoded-mtext" data-i18n="nav.page_layout.horizontal.fixed-layout">Ajouter une Agence</span>
                                                    <span class="pcoded-mcaret"></span>
                                                </a>
                                            </li>

                                        </ul>
                                    </li>

                                </ul>
                            </li>
                            <li class="pcoded-hasmenu">
                                <a href="javascript:void(0)">
                                    <span class="pcoded-micon"><i class="fa fa-file-archive-o"></i><b>P</b></span>
                                    <span class="pcoded-mtext" data-i18n="nav.page_layout.main">Centres</span>
                                    {{--                                    <span class="pcoded-badge label label-warning">NEW</span>--}}
                                    <span class="pcoded-mcaret"></span>
                                </a>
                                <ul class="pcoded-submenu">

                                    <li class=" pcoded-hasmenu">
                                        <a href="javascript:void(0)">
                                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                                            <span class="pcoded-mtext">Centres de Préarchives</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                        <ul class="pcoded-submenu">
                                            <li class=" ">
                                                <a href="{{route('structure.index','4')}}">
                                                    <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                                    <span class="pcoded-mtext" data-i18n="nav.page_layout.vertical.static-layout">Liste des Centres de Préarchives </span>
                                                    <span class="pcoded-mcaret"></span>
                                                </a>
                                            </li>
                                            <li class=" ">
                                                <a href="{{route('structure.create','4')}}">
                                                    <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                                    <span class="pcoded-mtext" data-i18n="nav.page_layout.vertical.header-fixed">Ajouter un Centre de Préarchive</span>
                                                    <span class="pcoded-mcaret"></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class=" pcoded-hasmenu">
                                        <a href="javascript:void(0)">
                                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                                            <span class="pcoded-mtext" data-i18n="nav.page_layout.horizontal.main">Centres des archives</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                        <ul class="pcoded-submenu">
                                            <li class=" ">
                                                <a href="{{route('structure.index','5')}}" >
                                                    <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                                    <span class="pcoded-mtext" data-i18n="nav.page_layout.horizontal.static-layout">Liste des Centres des archives</span>
                                                    <span class="pcoded-mcaret"></span>
                                                </a>
                                            </li>
                                            <li class=" ">
                                                <a href="{{route('structure.create','5')}}" >
                                                    <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                                    <span class="pcoded-mtext" data-i18n="nav.page_layout.horizontal.fixed-layout">Ajouter un Centre des archive</span>
                                                    <span class="pcoded-mcaret"></span>
                                                </a>
                                            </li>

                                        </ul>
                                    </li>

                                </ul>
                            </li>
                            <li class="pcoded-hasmenu">
                                <a href="javascript:void(0)">
                                    <span class="pcoded-micon"><i class="fa fa-file"></i><b>P</b></span>
                                    <span class="pcoded-mtext" data-i18n="nav.page_layout.main">Nomenclature</span>
                                    {{--                                    <span class="pcoded-badge label label-warning">NEW</span>--}}
                                    <span class="pcoded-mcaret"></span>
                                </a>
                                <ul class="pcoded-submenu">
                                    <li class=" ">
                                        <a href="{{route('nomenclature.index')}}">
                                            <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                                            <span class="pcoded-mtext">Liste des Nomenclatures</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                    <li class=" ">
                                        <a href="{{route('nomenclature.create')}}">
                                            <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                            <span class="pcoded-mtext" data-i18n="nav.page_layout.vertical.static-layout">Ajouter une Nomenclature</span>
                                            <span class="pcoded-mcaret"></span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            @endrole
                                @hasanyrole('responsable|employée')
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="fa fa-file"></i><b>P</b></span>
                                        <span class="pcoded-mtext" data-i18n="nav.page_layout.main">Documents</span>
                                        {{--                                    <span class="pcoded-badge label label-warning">NEW</span>--}}
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="{{route('document.index')}}">
                                                <span class="pcoded-micon"><i class="icon-pie-chart"></i></span>
                                                <span class="pcoded-mtext">Liste des Documents</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        @if(((Auth::user()->structure->type->id)!=4)&&((Auth::user()->structure->type->id)!=5))
                                        <li class=" ">
                                            <a href="{{route('document.create')}}">
                                                <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                                <span class="pcoded-mtext" data-i18n="nav.page_layout.vertical.static-layout">Ajouter un Document</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        @endif
                                        <li class=" ">
                                            <a href="{{route('versement.index')}}">
                                                <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                                <span class="pcoded-mtext" data-i18n="nav.page_layout.vertical.static-layout">Traiter les Documents</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        @if(((Auth::user()->structure->type->id)==1)||((Auth::user()->structure->type->id)==2)||((Auth::user()->structure->type->id)==3))
                                        <li class=" ">
                                            <a href="{{route('consultation.index')}}">
                                                <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                                <span class="pcoded-mtext" data-i18n="nav.page_layout.vertical.static-layout">Consultations</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                         @endif
                                    </ul>
                                </li>
                                @endhasanyrole

                                @role('responsable')
                                    <li class="pcoded-hasmenu">
{{--                                        <a href="{{route('demande.index')}}">--}}
{{--                                            <span class="pcoded-micon"><i class="fa fa-file"></i><b>P</b></span>--}}
{{--                                            <span class="pcoded-mtext" data-i18n="nav.page_layout.main">Demandes</span>--}}
{{--                                            --}}{{--                                    <span class="pcoded-badge label label-warning">NEW</span>--}}
{{--                                            <span class="pcoded-mcaret"></span>--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="fa fa-exclamation-circle"></i><b>S</b></span>
                                        <span class="pcoded-mtext" data-i18n="nav.page_layout.main">Demandes</span>
                                        {{--                                    <span class="pcoded-badge label label-warning">NEW</span>--}}
                                        <span class="pcoded-mcaret"></span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="{{route('demande.index')}}">
                                                <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                                <span class="pcoded-mtext" data-i18n="nav.page_layout.vertical.static-layout">Demandes Versement</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        @if(((Auth::user()->structure->type->id)=='4'))
                                        <li class=" ">
                                            <a href="{{route('consultation.index')}}">
                                                <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                                <span class="pcoded-mtext" data-i18n="nav.page_layout.vertical.static-layout">Demandes Consultation</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        @endif
                                        @if(((Auth::user()->structure->type->id)!='5'))
                                        <li class=" ">
                                            <a href="{{route('enlevement.index')}}">
                                                <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                                <span class="pcoded-mtext" data-i18n="nav.page_layout.vertical.static-layout">Plan d'Enlevement</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        @endif
                                        @if(((Auth::user()->structure->type->id)=='4')||((Auth::user()->structure->type->id)=='5'))
                                        <li class=" ">
                                            <a href="{{route('enlevement.demandes')}}">
                                                <span class="pcoded-micon"><i class="icon-chart"></i></span>
                                                <span class="pcoded-mtext" data-i18n="nav.page_layout.vertical.static-layout">Demandes d'Enlevement</span>
                                                <span class="pcoded-mcaret"></span>
                                            </a>
                                        </li>
                                        @endif
                                    </ul>
                                    </li>
                                @endrole

                        </ul>

                    </div>
                </nav>
                <div class="pcoded-content">
                    <div class="pcoded-inner-content">

                        <div class="main-body">
                            <div class="page-wrapper">
                                @yield('content')

                            </div>
                        </div>
{{--                        <div id="styleSelector">--}}

{{--                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Warning Section Starts -->
<!-- Older IE warning message -->
<!--[if lt IE 10]>
<div class="ie-warning">
    <h1>Warning!!</h1>
    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers
        to access this website.</p>
    <div class="iew-container">
        <ul class="iew-download">
            <li>
                <a href="http://www.google.com/chrome/">
                    <img src="{{asset('assets/images/browser/chrome.png')}}" alt="Chrome">
                    <div>Chrome</div>
                </a>
            </li>
            <li>
                <a href="https://www.mozilla.org/en-US/firefox/new/">
                    <img src="{{asset('assets/images/browser/firefox.png')}}" alt="Firefox">
                    <div>Firefox</div>
                </a>
            </li>
            <li>
                <a href="http://www.opera.com">
                    <img src="{{asset('assets/images/browser/opera.png')}}" alt="Opera">
                    <div>Opera</div>
                </a>
            </li>
            <li>
                <a href="https://www.apple.com/safari/">
                    <img src="{{asset('assets/images/browser/safari.png')}}" alt="Safari">
                    <div>Safari</div>
                </a>
            </li>
            <li>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                    <img src="{{asset('assets/images/browser/ie.png')}}" alt="">
                    <div>IE (9 & above)</div>
                </a>
            </li>
        </ul>
    </div>
    <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
<!-- Warning Section Ends -->
<!-- Required Jquery -->
<script type="text/javascript" src="{{asset('bower_components/jquery/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('bower_components/jquery-ui/js/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{asset('bower_components/popper.js/js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('bower_components/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- jquery slimscroll js -->
<script type="text/javascript" src="{{asset('bower_components/jquery-slimscroll/js/jquery.slimscroll.js')}}"></script>
<!-- modernizr js -->
<script type="text/javascript" src="{{asset('bower_components/modernizr/js/modernizr.js')}}"></script>
{{--<script type="text/javascript" src="{{asset('bower_components/modernizr/js/css-scrollbars.js')}}"></script>--}}
{{--<!-- am chart -->--}}
{{--<script src="{{asset('assets/pages/widget/amchart/amcharts.min.js')}}"></script>--}}
{{--<script src="{{asset('assets/pages/widget/amchart/serial.min.js')}}"></script>--}}
{{--<!-- Chart js -->--}}
{{--<script type="text/javascript" src="{{asset('bower_components/chart.js/js/Chart.js')}}"></script>--}}
{{--<!-- Todo js -->--}}
<!-- i18next.min.js -->
<script type="text/javascript" src="{{asset('bower_components/i18next/js/i18next.min.js')}}"></script>
<script type="text/javascript" src="{{asset('bower_components/i18next-xhr-backend/js/i18nextXHRBackend.min.js')}}"></script>
<script type="text/javascript" src="{{asset('bower_components/i18next-browser-languagedetector/js/i18nextBrowserLanguageDetector.min.js')}}"></script>
<script type="text/javascript" src="{{asset('bower_components/jquery-i18next/js/jquery-i18next.min.js')}}"></script>
<script src="{{asset('assets/js/pcoded.min.js')}}"></script>
<script src="{{asset('assets/js/demo-12.js')}}"></script>
<script src="{{asset('assets/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<!-- data-table js -->
<script src="{{asset('bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('bower_components/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('assets/pages/data-table/js/jszip.min.js')}}"></script>
<script src="{{asset('assets/pages/data-table/js/pdfmake.min.js')}}"></script>
<script src="{{asset('assets/pages/data-table/js/vfs_fonts.js')}}"></script>
<script src="{{asset('bower_components/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{asset('bower_components/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{asset('bower_components/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('bower_components/datatables.net-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('bower_components/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js')}}"></script>
<!-- Custom js -->
<script src="{{asset('assets/pages/data-table/js/data-table-custom.js')}}"></script>
{{--<script>--}}
{{--    $(document).ready(function () {--}}
{{--        $('#lala').modal('show');--}}
{{--    })--}}
{{--</script>--}}
<script src="{{asset('bower_components/iziToast/iziToast.min.js')}}" type="text/javascript"></script>
<script>
    @if($errors->all())
    @foreach($errors->all() as $message)
    iziToast.error({
        title: 'Erreur',
        message: '{{ $message }}',
        position: 'bottomRight'
    });
    @endforeach

    @elseif(session()->has('message'))
    iziToast.success({
        title: 'Succès',
        message: '{{ session()->get('message') }}',
        position: 'bottomRight'
    });

    @elseif(session()->has('error'))
    iziToast.error({
        title: 'Erreur',
        message: '{{ session()->get('error') }}',
        position: 'bottomRight'
    });
    @endif
</script>
@yield('scripts')
<script type="text/javascript" src="{{asset('assets/js/script.js')}}"></script>

</body>

</html>
