@extends('layout')
@section('nom')

@endsection
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont
                    icofont-file-alt
                        bg-c-blue"></i>
                    <div class="d-inline">
                        <h4>Liste des Sortie des documents</h4>
                        <span>Historiques Des <code style="text-transform: uppercase">Documents</code></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="/">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Liste des Sortie des documents</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <!-- DOM/Jquery table start -->
                <div class="card">

                    <div class="card-block">
                        <div class="table-responsive dt-responsive">
                            <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                <thead>
                                <tr>
                                    <th>Structure</th>
                                    <th>Utilisateur</th>
{{--                                    <th>Age</th>--}}
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($historiques as $historique)
                                    <tr>
                                        <td>{{$historique->structure->nom}}</td>
                                        <td>{{$historique->user->name}}
                                            <span class="badge badge-info">
                                                {{\App\User::find($historique->user->id)->getRoleNames()->first()}}
                                            </span>
                                        </td>
{{--                                        <td>{{$historique->age}}</td>--}}
                                        <td>{{$historique->action}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Structure</th>
                                    <th>Utilisateur</th>
{{--                                    <th>Age</th>--}}
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- DOM/Jquery table end -->
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        function ShowModalPrearchivage(documentid) {
            $('.doc_id').val(documentid);
            $('#prearchivage').modal('show');
        }
        function ShowModalSuppression(documentid) {
            $('.doc_id').val(documentid);
            $('#suppression').modal('show');
        }
    </script>
@endsection
