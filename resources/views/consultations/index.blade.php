@extends('layout')
@section('nom')

@endsection
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont
                    icofont-file-alt
                        bg-c-blue"></i>
                    <div class="d-inline">
                        <h4>Demande Consultation</h4>
                        <span>Liste Des <code style="text-transform: uppercase">Consultations</code></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="/">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Liste des Demandes de Consultation</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <!-- DOM/Jquery table start -->
                <div class="card">

                    <div class="card-block">
                        @if((Auth::user()->structure->type->id!=4)&&(Auth::user()->structure->type->id!=5))
                        <a href="{{route('consultation.create')}}" type="button" id="addRow" class="btn btn-primary m-b-20 float-lg-right">+ Créer Une Nouvelle Demande de Consultation
                        </a>
                        @endif
                        <div class="table-responsive dt-responsive">
                            <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                <thead>
                                <tr>
                                    <th>Numéro Document</th>
                                    <th>Raison</th>
                                    <th>Objet</th>
                                    <th>Statut</th>
                                    <th>Date</th>
                                    @if(Auth::user()->structure->type->id==4)
                                        <th>Utilisateur</th>
                                        <th>Structure</th>
                                        <th>Actions</th>
                                    @endif
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($consultations as $consultation)
                                    <tr>
                                        <td>{{$consultation->num}}</td>
                                        <td>{{$consultation->raison}}</td>
                                        <td>{{$consultation->objet}}</td>
                                        <td>
                                            @if($consultation->statut==0)
                                            <span class="badge badge-light badge-warning">En Attente</span>
                                            @elseif($consultation->statut==1)
                                            <span class="badge badge-light badge-success">Acceptée</span>
                                            @else
                                            <span class="badge badge-light badge-danger">Réfusée</span>
                                            @endif

                                        </td>
                                        <td>{{$consultation->date ? $consultation->date :'la demande n'.'\''.'as pas encore été traiter' }}</td>
                                        @if(Auth::user()->structure->type->id==4)
                                        <td>{{$consultation->user->name}}</td>
                                        <td>{{$consultation->user->structure->type->name.' '.$consultation->user->structure->adresse}}</td>
                                        <td>
                                            @if($consultation->statut==0)
                                            <button class="badge badge-md bg-success" data-toggle="modal"
                                                    data-target="#prearchivage{{$consultation->id}}">Accepter</button>
                                            <a href="#" data-toggle="modal"
                                               data-target="#suppression{{$consultation->id}}" class="badge badge-md bg-danger">Refuser</a>
                                            @endif
                                        </td>

                                        @endif
                                    </tr>
                                    <div class="modal fade" id="prearchivage{{$consultation->id}}" tabindex="-1" role="dialog">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Accepter la Demande de consultation</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <h5>Etes vous sûr de vouloir Accepter la Demande de consultation de ce document par {{$consultation->user->name}}?</h5>
                                                </div>
                                                <div class="modal-footer">
                                                    <form method="post" action="{{route('consultation.decision',$consultation->id)}}">
                                                        @csrf
                                                        <input type="hidden" name="decision" value="1">
{{--                                                        <input type="hidden" name="consultation" value="{{$consultation->id}}">--}}
                                                        <label>sélectionner  la date </label>
                                                        <input type="date" name="date" >
                                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Non</button>
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light ">Oui</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal fade" id="suppression{{$consultation->id}}" tabindex="-1" role="dialog">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Confirmation de Suppression</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <h5>Etes vous sûr de vouloir Refuser cette demande de consultation ?</h5>
                                                </div>
                                                <div class="modal-footer">
                                                    <form method="post" action="{{route('consultation.decision',$consultation->id)}}">
                                                        @csrf
                                                        <input type="hidden" name="decision" value="0">
                                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Non</button>
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light ">Oui</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Numéro Document</th>
                                    <th>Raison</th>
                                    <th>Objet</th>
                                    <th>Statut</th>
                                    <th>Date</th>
                                    @if(Auth::user()->structure->type->id==4)
                                    <th>Utilisateur</th>
                                    <th>Structure</th>
                                    <th>Actions</th>
                                    @endif
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- DOM/Jquery table end -->
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        function ShowModalPrearchivage(documentid) {
            $('.doc_id').val(documentid);
            $('#prearchivage').modal('show');
        }
        function ShowModalSuppression(documentid) {
            $('.doc_id').val(documentid);
            $('#suppression').modal('show');
        }
    </script>
@endsection
