@extends('layout')
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont
                    icofont-file-alt
                        bg-c-blue"></i>
                    <div class="d-inline">
                        <h4>Consultation</h4>
                        <span>Créer Une Demande de <code style="text-transform: uppercase">Consultation</code></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="/">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Créer Une Demande de Consultation</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <!-- Basic Form Inputs card start -->
                <div class="card">

                    <div class="card-block">
                        <h4 class="sub-title">Formulaire de création</h4>
                        <form method="post" action="{{route('consultation.store')}}">
                            @csrf
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Numéro document</label>
                                <div class="col-sm-10">
                                    <input name="num" type="text" class="form-control" placeholder="Numéro document">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Raison</label>
                                <div class="col-sm-10">
                                    <input name="raison" type="text" class="form-control" placeholder="Raison">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label">Objet</label>
                                <div class="col-sm-10">
                                    <textarea name="objet" type="text" class="form-control" placeholder="Objet">
                                    </textarea>
                                </div>
                            </div>

                            <div class="text-center">
                            <button type="submit" class="col-md-3 btn btn-out-dashed btn-success btn-square">Créer</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- Basic Form Inputs card end -->
            </div>
        </div>

    </div>
@endsection
