@extends('layout')
@section('nom')

@endsection
@section('content')
    <div class="page-header card">
        <div class="row align-items-end">
            <div class="col-lg-8">
                <div class="page-header-title">
                    <i class="icofont
                    icofont-file-alt
                        bg-c-blue"></i>
                    <div class="d-inline">
                        <h4>Document</h4>
                        <span>Liste Des <code style="text-transform: uppercase">Demandes</code></span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                    <ul class="breadcrumb-title">
                        <li class="breadcrumb-item">
                            <a href="/">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Liste des Demandes</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page-body">
        <div class="row">
            <div class="col-sm-12">
                <!-- DOM/Jquery table start -->
                <div class="card">

                    <div class="card-block">
                        @if((Auth::user()->structure->type->id)!=5)
                        <a href="{{route('enlevement.create')}}" type="button" id="addRow" class="btn btn-primary m-b-20 float-lg-right">+ Ajouter a un plan d'enlevement
                        </a>
                        @endif
                        <div class="table-responsive dt-responsive">
                            <table id="dom-jqry" class="table table-striped table-bordered nowrap">
                                <thead>
                                <tr>
                                    <th>Utilisateur</th>
                                    <th>Structure</th>
                                    <th>Numéro Documents</th>
                                    <th>Nomenclature</th>
                                    <th>Demande</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($versements as $versement)
                                    <tr>
                                        <td>{{$versement->document->user->name}}</td>
                                        <td>{{$versement->document->user->structure->type->name}}|{{$versement->document->user->structure->nom}}</td>
                                        <td>{{$versement->document->num}}</td>
                                        <td>{{$versement->document->nomenclature->designation}}</td>
                                        <td>{{$versement->decision==0 ? 'supprimer' : 'préarchivage'}}</td>

                                        @if(Auth::user()->structure->type->id==5)
                                        <td>
                                            <a href="#" class="badge badge-md bg-success"
                                               data-toggle="modal"
                                               data-target="#accepter{{$versement->id}}">Accepter</a>
                                            <a href="#" data-toggle="modal"
                                               data-target="#refuser{{$versement->id}}" class="badge badge-md bg-danger">Refuser</a>                                        </td>
                                        @else
                                            <td>
                                            @if($versement->decision==0)
                                                <a href="#" class="badge badge-md bg-success"
                                                   data-toggle="modal"
                                                   data-target="#accepter{{$versement->id}}">Accepter</a>
                                                <a href="#" data-toggle="modal"
                                                   data-target="#refuser{{$versement->id}}" class="badge badge-md bg-danger">Refuser</a>
                                            @else
                                                    <a href="#" data-toggle="modal"
                                                       data-target="#refuser{{$versement->id}}" class="badge badge-md bg-danger">Refuser</a>
                                            @endif
                                            </td>
                                        @endif
                                    </tr>
                                    <div class="modal fade" id="accepter{{$versement->id}}" tabindex="-1" role="dialog">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Confirmation de
                                                        @if($versement->decision==0)
                                                            Suppression
                                                        @elseif($versement->decision==1)
                                                        {{ $text==1 ? 'Préarchivage' : 'Archivage'}}
                                                        @endif
                                                    </h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <h5>Etes vous sûr de vouloir
                                                        @if($versement->decision==0)
                                                            Supprimer
                                                        @elseif($versement->decision==1)
                                                            {{ $text==1 ? 'préarchiver' : 'archiver'}}
                                                        @endif
                                                     ce document ?</h5>
                                                </div>
                                                <div class="modal-footer">
                                                    <form method="post" action="{{route('demande.decision',$versement->id)}}">
                                                        @csrf
                                                        <input type="hidden" name="decision" value="1">
{{--                                                        <input type="hidden" name="versement_id" class="doc_id" value="{{$versement->id}}">--}}
                                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Non</button>
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light ">Oui</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal fade" id="refuser{{$versement->id}}" tabindex="-1" role="dialog">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Confirmation de
                                                        @if($versement->decision==0)
                                                            {{ $text==1 ? 'préarchivage' :'archivage'}}
                                                        @elseif($versement->decision==1)
                                                            Supprimer
                                                        @endif
                                                        </h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <h5>Etes vous sûr de vouloir
                                                        @if($versement->decision==0)
                                                            {{ $text==1 ? 'préarchiver' : 'archiver'}}
                                                        @elseif($versement->decision==1)
                                                            Supprimer
                                                        @endif
                                                        ce document ?</h5>
                                                </div>
                                                <div class="modal-footer">
                                                    <form method="post" action="{{route('demande.decision',$versement->id)}}">
                                                        @csrf
                                                        <input type="hidden" name="decision" value="0">
{{--                                                        <input type="hidden" name="versement_id" class="doc_id" value="">--}}
                                                        <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">Non</button>
                                                        <button type="submit" class="btn btn-primary waves-effect waves-light ">Oui</button>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th>Utilisateur</th>
                                    <th>Structure</th>
                                    <th>Numéro Documents</th>
                                    <th>Nomenclature</th>
                                    <th>Actions</th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- DOM/Jquery table end -->
            </div>
        </div>
    </div>

@endsection
@section('scripts')
{{--    <script>--}}
{{--        // function ShowModalAccepter(documentid) {--}}
{{--        //     $('.doc_id').val(documentid);--}}
{{--        //     $('#accepter').modal('show');--}}
{{--        // }--}}
{{--        // function ShowModalRefuser(documentid) {--}}
{{--        //     $('.doc_id').val(documentid);--}}
{{--        //     $('#refuser').modal('show');--}}
{{--        }--}}
{{--    </script>--}}
@endsection
