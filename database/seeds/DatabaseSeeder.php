<?php

use App\User;
use Illuminate\Database\Seeder;
//use Spatie\Permission\Contracts\Role;
use Spatie\Permission\Models\Role;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert([
            'name' => 'Agence',
        ]);
        DB::table('types')->insert([
            'name' => 'Direction Regionale',
        ]);
        DB::table('types')->insert([
            'name' => 'Structure Central',
        ]);
        DB::table('types')->insert([
            'name' => 'Centre de Prearchivage',
        ]);
        DB::table('types')->insert([
            'name' => 'Centre d\'Archivage',
        ]);

//        DB::table('structures')->insert([
//            'nom' => 'Direction Commerciale',
//            'code' => 'XuI45mp',
//            'adresse' => '34 Rue palestine',
//            'age1' => 'Bureau',
//            'age2' => '3',
//            'type_id' => '2',
//            'region_id' => '1',
//        ]);
//        DB::table('structures')->insert([
//            'nom' => 'Agence ',
//            'code' => 'XuI45mp',
//            'adresse' => '10 Rue De Chine',
//            'age1' => 'Bureau',
//            'age2' => '3',
//            'type_id' => '1',
//            'region_id' => '2',
//        ]);
//        DB::table('structures')->insert([
//            'nom' => 'préarchive ',
//            'code' => 'XuI45mp',
//            'adresse' => '10 Rue De Chine',
//            'age1' => 'Bureau',
//            'age2' => '3',
//            'type_id' => '4',
//            'region_id' => '2',
//        ]);
        DB::table('regions')->insert(['name' => 'Tunis']);
        DB::table('regions')->insert(['name' => 'Ariana']);
        DB::table('regions')->insert(['name' => 'Beja']);
        DB::table('regions')->insert(['name' => 'BenArous']);
        DB::table('regions')->insert(['name' => 'Bizerte']);
        DB::table('regions')->insert(['name' => 'Gabes']);
        DB::table('regions')->insert(['name' => 'Gafsa']);
        DB::table('regions')->insert(['name' => 'Jendouba']);
        DB::table('regions')->insert(['name' => 'Kairouan']);
        DB::table('regions')->insert(['name' => 'Kasserine']);
        DB::table('regions')->insert(['name' => 'Kebili']);
        DB::table('regions')->insert(['name' => 'Kef']);
        DB::table('regions')->insert(['name' => 'Mahdia']);
        DB::table('regions')->insert(['name' => 'Manouba']);
        DB::table('regions')->insert(['name' => 'Medenine']);
        DB::table('regions')->insert(['name' => 'Monastir']);
        DB::table('regions')->insert(['name' => 'Nabeul']);
        DB::table('regions')->insert(['name' => 'Sfax']);
        DB::table('regions')->insert(['name' => 'SidiBouzid']);
        DB::table('regions')->insert(['name' => 'Siliana']);
        DB::table('regions')->insert(['name' => 'Sousse']);
        DB::table('regions')->insert(['name' => 'Tataouine']);
        DB::table('regions')->insert(['name' => 'Tozeur']);
        DB::table('regions')->insert(['name' => 'Zaghouan']);

//        DB::table('nomenclatures')->insert([
//            'code' => '45Po78ad',
//            'designation' => 'DAG finance',
//            'age1' =>json_encode(array('year' => 1, 'month' => 2, 'day' => 3)),
//            'age2' =>json_encode(array('year' => 1, 'month' => 2, 'day' => 3)),
//            'age3' =>json_encode(array('year' => 1, 'month' => 2, 'day' => 3)),
//            'type_id' => '1',
//
//        ]);
//
//        DB::table('nomenclatures')->insert([
//            'code' => '99aOJZm',
//            'designation' => 'DAG Commerciale',
//            'age1' =>json_encode(array('year' => 3, 'month' => 0, 'day' => 0)),
//            'age2' =>json_encode(array('year' => 0, 'month' => 0, 'day' => 3)),
//            'age3' =>json_encode(array('year' => 1, 'month' => 0, 'day' => 3)),
//            'type_id' => '1',
//        ]);

        User::create([
            'name' => 'Mariem Nefzaoui',
            'email' => 'admin@gmail.com',
            'password' => bcrypt(123456),
//            'structure_id' => '1',
            'matricule' => '456s4dMIRN',
        ]);
//        User::create([
//            'name' => 'saif bk',
//            'email' => 'saif@gmail.com',
//            'password' => bcrypt('123456'),
//            'structure_id' => '2',
//            'matricule' => 'sasasP456AZ',
//        ]);
//        User::create([
//            'name' => 'khalil',
//            'email' => 'khalil@gmail.com',
//            'password' => bcrypt('123456'),
//            'structure_id' => '2',
//            'matricule' => 'samjilme78465uo',
//        ]);
//        User::create([
//            'name' => 'nejib',
//            'email' => 'nejib@gmail.com',
//            'password' => bcrypt('123456'),
//            'structure_id' => '3',
//            'matricule' => 'samjilme78465uo',
//        ]);

        Role::create(['name' => 'admin']);
        Role::create(['name' => 'responsable']);
        Role::create(['name' => 'employée']);

        $user=User::find('1');
        $user->assignRole('admin');
//        $user=User::find('2');
//        $user->assignRole('responsable');
//        $user=User::find('3');
//        $user->assignRole('employée');
//        $user=User::find('4');
//        $user->assignRole('responsable');



    }
//    public function run()
//    {
//        DB::table('nomenclatures')->insert([
//            'code' => '45Po78ad',
//            'designation' => 'DAG finance',
//            'age1' =>json_encode(array('year' => 1, 'month' => 2, 'day' => 3)),
//            'age2' =>json_encode(array('year' => 1, 'month' => 2, 'day' => 3)),
//            'age3' =>json_encode(array('year' => 1, 'month' => 2, 'day' => 3)),
//            'type_id' => '1',
//
//        ]);
//    }

}
