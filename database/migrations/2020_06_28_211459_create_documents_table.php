<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->bigInteger('num');
            $table->bigInteger('pages');
            $table->bigInteger('nbrdoc');
            $table->string('chapitre');
            $table->date('age1');
            $table->date('age2')->nullable();
            $table->date('age3')->nullable();
            $table->string('statut')->default('1')->nullable();
            $table->string('traitement')->default('0')->nullable();
            $table->string('versement')->default('0')->nullable();
            $table->unsignedBigInteger('versement_id')->nullable();
            $table->foreign('versement_id')->references('id')->on('versements');
            $table->unsignedBigInteger('nomenclature_id');
            $table->foreign('nomenclature_id')->references('id')->on('nomenclatures');
            $table->unsignedBigInteger('structure_id');
            $table->foreign('structure_id')->references('id')->on('structures');
            $table->unsignedBigInteger('enlevement_id')->nullable();
            $table->foreign('enlevement_id')->references('id')->on('enlevements');
            $table->boolean('enlevement')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
