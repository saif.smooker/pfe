<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Prearchive;
use Faker\Generator as Faker;

$factory->define(Prearchive::class, function (Faker $faker) {
    return [
        'nom' => $faker->name,
        'code' => $faker->postcode,
        'adresse' => $faker->address,
        'region_id' => '1',
    ];
});
