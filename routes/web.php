<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('layout');
//});
//Route::resource('agences', 'AgenceController');
//Route::resource('regionals', 'RegionalController');
//Route::resource('centrals', 'CentralController');
//Route::resource('structures/{type_id}', 'StructureController');
Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::middleware(['web'])->group(function () {



    Route::get('historiques', 'HistoryController@index');
    Route::resource('user', 'UserController');
    Route::put('user/{id}/edit', 'UserController@password')->name('user.password');
    Route::resource('document', 'DocumentController');
    Route::resource('nomenclature', 'NomenclatureController');
    Route::resource('calendrier', 'CalendrierController');
    Route::get('versement', 'VersementController@index')->name('versement.index');
    Route::post('versement', 'VersementController@decision')->name('versement.decision');
    Route::get('demande', 'DemandeController@index')->name('demande.index');
    Route::post('demande/{id}', 'DemandeController@decision')->name('demande.decision');

    Route::get('/ajax/structurestype/{type?}', 'UserController@StructuresType')->name('ajax.structurestype');
    Route::get('structures/{type_id}', 'StructureController@index')->name('structure.index');
    Route::get('structures/{type_id}/create', 'StructureController@create')->name('structure.create');
    Route::post('structures/{type_id}', 'StructureController@store')->name('structure.store');
    Route::get('structures/{type_id}/{structure}/edit', 'StructureController@edit')->name('structure.edit');
    Route::put('structures/{type_id}/{structure}', 'StructureController@update')->name('structure.update');
    Route::get('structures/{type_id}/{structure}', 'StructureController@show')->name('structure.show');
    Route::delete('structures/{type_id}/{structure}', 'StructureController@destroy')->name('structure.delete');

    Route::get('consultation', 'ConsultationController@index')->name('consultation.index');
    Route::post('consultation/create', 'ConsultationController@store')->name('consultation.store');
    Route::get('consultation/create', 'ConsultationController@create')->name('consultation.create');
    Route::post('consultation/{id}', 'ConsultationController@decision')->name('consultation.decision');


    Route::get('enlevement/demandes', 'EnlevementController@demandes')->name('enlevement.demandes');
    Route::put('enlevement/demandes/{enlevement}', 'EnlevementController@date')->name('enlevement.date');
    Route::get('enlevement/calendar', 'EnlevementController@calendar')->name('enlevement.calendar');
    Route::resource('enlevement', 'EnlevementController');


});




